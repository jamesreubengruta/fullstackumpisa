import 'package:flutter/material.dart';
import 'package:umpisa_flutter/core/presentation/providers/dashboard_controller.dart';

class Footer extends StatefulWidget {
  const Footer({super.key, required this.controller});
  final DashboardController controller;
  @override
  State<Footer> createState() => _FooterState();
}

class _FooterState extends State<Footer> {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
        selectedFontSize: 0,
        type: BottomNavigationBarType.fixed,
        currentIndex: widget.controller.currentIndex,
        showSelectedLabels: false,
        backgroundColor: Colors.grey,
        elevation: 0,
        onTap: widget.controller.changeIndex,
        items: const [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.note_alt_sharp,
                color: Colors.white,
              ),
              activeIcon: Icon(
                Icons.note_alt_sharp,
                color: Colors.amber,
              ),
              label: 'Notes',
              tooltip: ''),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.person,
                color: Colors.white,
              ),
              activeIcon: Icon(
                Icons.person,
                color: Colors.amber,
              ),
              label: 'Profile',
              backgroundColor: Colors.white,
              tooltip: '')
        ]);
  }
}
