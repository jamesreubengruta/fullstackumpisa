import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:umpisa_flutter/core/presentation/providers/dashboard_controller.dart';
import 'package:umpisa_flutter/modules/dashboard/presentation/footer.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({super.key});
  static const routeName = '/dashboard';
  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Consumer<DashboardController>(builder: (_, controller, __) {
      return Scaffold(
        extendBody: true,
        body: SafeArea(
          child: IndexedStack(
              index: controller.currentIndex, children: controller.screens),
        ),
        bottomNavigationBar: Footer(
          controller: controller,
        ),
      );
    });
  }
}
