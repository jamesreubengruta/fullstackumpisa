import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:umpisa_flutter/modules/notes/application/bloc/note_event.dart';
import 'package:umpisa_flutter/modules/notes/application/bloc/note_state.dart';
import 'package:umpisa_flutter/modules/notes/domain/usecases/add_note_uc.dart';
import 'package:umpisa_flutter/modules/notes/domain/usecases/delete_note_uc.dart';
import 'package:umpisa_flutter/modules/notes/domain/usecases/get_notes_uc.dart';
import 'package:umpisa_flutter/modules/notes/domain/usecases/update_note_uc.dart';

class NoteBloc extends Bloc<NoteEvent, NoteState> {
  NoteBloc({
    required this.deleteNoteUC,
    required this.getNotesUC,
    required this.addNoteUC,
    required this.updateNoteUC,
  }) : super(const NoteInitialState()) {
    on<GetNoteEvent>(_mapGetNoteEventToState);
    on<DeleteNoteEvent>(_mapDeleteNoteEventToState);
    on<AddNoteEvent>(_mapAddNoteEventToState);
    on<UpdateNoteEvent>(_mapUpdateNoteEventToState);
  }

  final DeleteNoteUC deleteNoteUC;
  final GetNotesUC getNotesUC;
  final UpdateNoteUC updateNoteUC;
  final AddNoteUC addNoteUC;

  void _mapGetNoteEventToState(
      GetNoteEvent event, Emitter<NoteState> emit) async {
    emit(const NoteLoadingState());
    final result = await getNotesUC.call(const GetNotesParams(name: ''));
    result.fold(
      (failure) => emit(NoteErrorState(message: failure.errorMessage)),
      (data) => emit(NoteGetSuccessState(model: data)),
    );
  }

  void _mapDeleteNoteEventToState(
      DeleteNoteEvent event, Emitter<NoteState> emit) async {
    emit(const NoteLoadingState());
    final result = await deleteNoteUC.call(DeleteNoteParams(id: event.id));
    result.fold(
      (failure) => emit(NoteErrorState(message: failure.errorMessage)),
      (data) => emit(NoteSuccessState(model: data)),
    );
  }

  void _mapAddNoteEventToState(
      AddNoteEvent event, Emitter<NoteState> emit) async {
    emit(const NoteLoadingState());
    final result = await addNoteUC.call(AddNoteParams(
        title: event.title, note: event.note, description: event.description));
    result.fold(
      (failure) => emit(NoteErrorState(message: failure.errorMessage)),
      (data) => emit(NoteSuccessState(model: data)),
    );
  }

  void _mapUpdateNoteEventToState(
      UpdateNoteEvent event, Emitter<NoteState> emit) async {
    emit(const NoteLoadingState());
    final result = await updateNoteUC.call(UpdateNoteParams(
        title: event.title,
        note: event.note,
        description: event.description,
        id: event.id));
    result.fold(
      (failure) => emit(NoteErrorState(message: failure.errorMessage)),
      (data) => emit(NoteUpdateSuccessState(model: data)),
    );
  }
}
