import 'package:equatable/equatable.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';

sealed class NoteState extends Equatable {
  const NoteState();

  @override
  List<Object> get props => [];
}

final class NoteInitialState extends NoteState {
  const NoteInitialState();
}

final class NoteLoadingState extends NoteState {
  const NoteLoadingState();
}

final class NoteUpdateSuccessState extends NoteState {
  const NoteUpdateSuccessState({required this.model});
  final NotesModel model;
  @override
  List<Object> get props => [model];
}

final class NoteGetSuccessState extends NoteState {
  const NoteGetSuccessState({required this.model});
  final List<NotesModel> model;
  @override
  List<Object> get props => [model];
}

final class NoteSuccessState extends NoteState {
  const NoteSuccessState({required this.model});
  final NotesModel model;
  @override
  List<Object> get props => [model];
}

class NoteErrorState extends NoteState {
  final String message;

  const NoteErrorState({required this.message});

  @override
  List<Object> get props => [message];
}
