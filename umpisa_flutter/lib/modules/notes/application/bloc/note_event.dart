import 'package:equatable/equatable.dart';

sealed class NoteEvent extends Equatable {
  const NoteEvent();

  @override
  List<Object> get props => [];
}

class GetNoteEvent extends NoteEvent {
  const GetNoteEvent();
  @override
  List<Object> get props => [];
}

class DeleteNoteEvent extends NoteEvent {
  final String id;
  const DeleteNoteEvent(this.id);
  @override
  List<Object> get props => [id];
}

class AddNoteEvent extends NoteEvent {
  final String title;
  final String note;
  final String date;
  final String description;

  const AddNoteEvent(
      {required this.title,
      required this.note,
      required this.date,
      required this.description});
  @override
  List<Object> get props => [title, note, date, description];
}

class UpdateNoteEvent extends NoteEvent {
  final String id;
  final String title;
  final String note;
  final String date;
  final String description;

  const UpdateNoteEvent(
      {required this.title,
      required this.id,
      required this.note,
      required this.date,
      required this.description});
  @override
  List<Object> get props => [];
}
