import 'package:umpisa_flutter/core/core.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';

abstract class NotesRepository {
  const NotesRepository();

  ResulT<List<NotesModel>> getNotes({required String name});
  ResulT<NotesModel> deleteNote({required String id});
  ResulT<NotesModel> updateNote(
      {required String id, required String title, required String note});
  ResulT<NotesModel> addNote(
      {required String title,
      required String note,
      required String description});
}
