import 'package:equatable/equatable.dart';

class NotesModel extends Equatable {
  final String id;
  final String title;
  final String note;
  final String description;
  final String date;

  const NotesModel(
      {required this.id,
      required this.title,
      required this.note,
      required this.description,
      required this.date});

  const NotesModel.empty()
      : id = '',
        title = '',
        note = '',
        description = '',
        date = '';

  @override
  List<Object?> get props => [id, title];
}
