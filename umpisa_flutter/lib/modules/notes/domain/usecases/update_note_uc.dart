import 'package:equatable/equatable.dart';
import 'package:umpisa_flutter/core/usecases/usecase.dart';
import 'package:umpisa_flutter/core/utils/typedef.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';
import 'package:umpisa_flutter/modules/notes/domain/repository/notes_repo.dart';

class UpdateNoteUC extends UsecaseParams<NotesModel, UpdateNoteParams> {
  final NotesRepository _repo;

  UpdateNoteUC({required NotesRepository provider}) : _repo = provider;

  @override
  ResulT<NotesModel> call(UpdateNoteParams p) async {
    return _repo.updateNote(id: p.id, title: p.title, note: p.note);
  }
}

class UpdateNoteParams extends Equatable {
  final String id;
  final String title;
  final String note;
  final String description;

  const UpdateNoteParams(
      {required this.id,
      required this.title,
      required this.note,
      required this.description});

  @override
  List<Object?> get props => [id];
}
