import 'package:equatable/equatable.dart';
import 'package:umpisa_flutter/core/usecases/usecase.dart';
import 'package:umpisa_flutter/core/utils/typedef.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';
import 'package:umpisa_flutter/modules/notes/domain/repository/notes_repo.dart';

class DeleteNoteUC extends UsecaseParams<void, DeleteNoteParams> {
  final NotesRepository _repo;

  DeleteNoteUC({required NotesRepository provider}) : _repo = provider;

  @override
  ResulT<NotesModel> call(DeleteNoteParams p) async {
    return _repo.deleteNote(id: p.id);
  }
}

class DeleteNoteParams extends Equatable {
  final String id;
  const DeleteNoteParams({required this.id});

  const DeleteNoteParams.empty() : this(id: '');

  @override
  List<Object?> get props => [id];
}
