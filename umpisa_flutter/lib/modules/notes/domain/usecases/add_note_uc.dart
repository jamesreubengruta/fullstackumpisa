import 'package:equatable/equatable.dart';
import 'package:umpisa_flutter/core/usecases/usecase.dart';
import 'package:umpisa_flutter/core/utils/typedef.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';
import 'package:umpisa_flutter/modules/notes/domain/repository/notes_repo.dart';

class AddNoteUC extends UsecaseParams<NotesModel, AddNoteParams> {
  final NotesRepository _repo;

  AddNoteUC({required NotesRepository provider}) : _repo = provider;

  @override
  ResulT<NotesModel> call(AddNoteParams p) async {
    return _repo.addNote(
        description: p.description, title: p.title, note: p.note);
  }
}

class AddNoteParams extends Equatable {
  final String description;
  final String title;
  final String note;

  const AddNoteParams({
    required this.description,
    required this.title,
    required this.note,
  });

  @override
  List<Object?> get props => [description];
}
