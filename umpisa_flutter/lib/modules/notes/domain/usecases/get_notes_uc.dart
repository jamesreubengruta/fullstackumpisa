import 'package:equatable/equatable.dart';
import 'package:umpisa_flutter/core/usecases/usecase.dart';
import 'package:umpisa_flutter/core/utils/typedef.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';
import 'package:umpisa_flutter/modules/notes/domain/repository/notes_repo.dart';

class GetNotesUC extends UsecaseParams<List<NotesModel>, GetNotesParams> {
  final NotesRepository _repo;

  GetNotesUC({required NotesRepository provider}) : _repo = provider;

  @override
  ResulT<List<NotesModel>> call(GetNotesParams p) async {
    return _repo.getNotes(name: p.name);
  }
}

class GetNotesParams extends Equatable {
  final String name;
  const GetNotesParams({required this.name});

  const GetNotesParams.empty() : this(name: '');

  @override
  List<Object?> get props => [name];
}
