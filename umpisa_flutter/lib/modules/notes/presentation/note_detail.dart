import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:umpisa_flutter/core/presentation/tools/contextual.dart';
import 'package:umpisa_flutter/core/presentation/widgets/back_header.dart';
import 'package:umpisa_flutter/modules/notes/application/bloc/note_bloc.dart';
import 'package:umpisa_flutter/modules/notes/application/bloc/note_event.dart';
import 'package:umpisa_flutter/modules/notes/application/bloc/note_state.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';

class NoteDetailScreen extends StatelessWidget {
  final String? id;
  final String? title;
  final String? note;
  final String? description;
  final String? date;
  static const routeName = '/detail_note';

  const NoteDetailScreen({
    super.key,
    this.id,
    this.title,
    this.note,
    this.description,
    this.date,
  });

  @override
  Widget build(BuildContext context) {
    late NotesModel model;
    void delete() {
      context.read<NoteBloc>().add(DeleteNoteEvent(id.toString()));
    }

    return BlocConsumer<NoteBloc, NoteState>(
      listener: (context, state) {
        if (state is NoteSuccessState) {
          model = state.model;
          ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text("Deletion of model successful! ")));

          Future.delayed(Duration.zero, () {
            context.pop();
          });
        } else if (state is NoteErrorState) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text('Note deletion failed: ${state.message}')));
        }
      },
      builder: (BuildContext context, NoteState state) {
        return Scaffold(
          appBar: CustomHeader(
            title: 'Note Detail',
            onBackButtonPressed: () {
              context.pop();
            },
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'ID: $id',
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(height: 16),
                  Text(
                    'Title: $title',
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text('Note: $note'),
                  const SizedBox(height: 8),
                  Text('Description: $description'),
                  const SizedBox(height: 8),
                  Text(
                    'Date: $date',
                    style: const TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                ],
              ),
            ),
          ),
          floatingActionButton: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              FloatingActionButton(
                onPressed: () {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text(
                          'This feature is not implemented yet. Coming soon!')));
                },
                backgroundColor: Colors.amber,
                child: const Icon(Icons.edit),
              ),
              const SizedBox(height: 16), // Add some space between FABs
              FloatingActionButton(
                onPressed: () {
                  delete();
                },
                backgroundColor: Colors.red,
                child: const Icon(Icons.delete),
              ),
            ],
          ),
        );
      },
    );
  }
}
