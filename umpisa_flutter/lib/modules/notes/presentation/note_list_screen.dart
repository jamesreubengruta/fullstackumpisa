import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:umpisa_flutter/ccc/widgets/note_item.dart';
import 'package:umpisa_flutter/core/presentation/tools/contextual.dart';
import 'package:umpisa_flutter/core/presentation/widgets/back_header.dart';
import 'package:umpisa_flutter/modules/notes/application/bloc/note_bloc.dart';
import 'package:umpisa_flutter/modules/notes/application/bloc/note_event.dart';
import 'package:umpisa_flutter/modules/notes/application/bloc/note_state.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';
import 'package:umpisa_flutter/modules/notes/presentation/note_create_screen.dart';
import 'package:umpisa_flutter/modules/notes/presentation/note_detail.dart';

class NoteListScreen extends StatefulWidget {
  const NoteListScreen({super.key});
  static const routeName = '/list_note';

  @override
  State<NoteListScreen> createState() => _NoteListScreenState();
}

class _NoteListScreenState extends State<NoteListScreen> {
  bool loading = true;
  List<NotesModel> items = [];
  @override
  void dispose() {
    super.dispose();
  }

  void fetchNotes() {
    context.read<NoteBloc>().add(const GetNoteEvent());
  }

  @override
  void initState() {
    super.initState();
    fetchNotes();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NoteBloc, NoteState>(
      listener: (context, state) {
        if (state is NoteGetSuccessState) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("models:${state.model.length} retrieved")));
          items = state.model;
          loading = false;
        } else if (state is NoteErrorState) {
          loading = false;
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text('Note retrieval failed: ${state.message}')));
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: CustomHeader(
            showBackbutton: false,
            title: 'My Notes',
            onBackButtonPressed: () {},
          ),
          body: Container(
            constraints: const BoxConstraints.expand(),
            decoration: const BoxDecoration(
              color: Colors.amber,
            ),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Stack(
                        children: [
                          ListView.builder(
                            itemCount: items.length,
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                onTap: () => context.push(NoteDetailScreen(
                                    id: items[index].id,
                                    title: items[index].title,
                                    note: items[index].note,
                                    description: items[index].description,
                                    date: items[index].date)),
                                child: NoteItemWidget(
                                    id: items[index].id,
                                    title: items[index].title,
                                    note: items[index].note,
                                    description: items[index].description,
                                    date: items[index].date),
                              );
                            },
                          ),
                          Positioned(
                            bottom: 0,
                            right: 0,
                            child: FloatingActionButton(
                              onPressed: () {
                                context.push(const NoteCreateScreen());
                              },
                              tooltip: 'Add',
                              backgroundColor: Colors.white,
                              child: const Icon(Icons.add), // Optional tooltip
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
