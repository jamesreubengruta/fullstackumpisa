import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:umpisa_flutter/core/presentation/tools/contextual.dart';
import 'package:umpisa_flutter/core/presentation/widgets/back_header.dart';
import 'package:umpisa_flutter/modules/notes/application/bloc/note_bloc.dart';
import 'package:umpisa_flutter/modules/notes/application/bloc/note_event.dart';
import 'package:umpisa_flutter/modules/notes/application/bloc/note_state.dart';

class NoteCreateScreen extends StatefulWidget {
  const NoteCreateScreen({super.key});
  static const routeName = '/create_note';

  @override
  State<NoteCreateScreen> createState() => _NoteCreateScreenState();
}

class _NoteCreateScreenState extends State<NoteCreateScreen> {
  bool loading = true;
  TextEditingController titleController = TextEditingController(text: '');
  TextEditingController noteController = TextEditingController(text: '');
  TextEditingController descriptionController = TextEditingController(text: '');

  @override
  void dispose() {
    super.dispose();
    titleController.dispose();
    noteController.dispose();
    descriptionController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    void register() {
      String title = titleController.text;
      String note = noteController.text;
      String description = descriptionController.text;

      if (title.isNotEmpty && note.isNotEmpty && description.isNotEmpty) {
        context.read<NoteBloc>().add(AddNoteEvent(
              title: title,
              note: note,
              date: '',
              description: description,
            ));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text("Fill all the fields!")));
      }
    }

    return BlocConsumer<NoteBloc, NoteState>(
      listener: (context, state) {
        if (state is NoteSuccessState) {
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text("id:${state.model.id} has been added")));
          loading = false;
          Future.delayed(Duration.zero, () {
            context.pop();
          });
        } else if (state is NoteErrorState) {
          loading = false;
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text('Note creation failed: ${state.message}')));
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: CustomHeader(
            title: 'Create a Note',
            onBackButtonPressed: () {
              context.pop();
            },
          ),
          body: Container(
            constraints: const BoxConstraints.expand(),
            decoration: const BoxDecoration(
              color: Colors.amber,
            ),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextField(
                      controller: titleController,
                      decoration: const InputDecoration(
                        hintText: 'Title',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    TextField(
                      minLines: 3,
                      maxLines: 10,
                      controller: noteController,
                      decoration: const InputDecoration(
                        hintText: 'Note',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    const SizedBox(
                        height: 16.0), // Add some space between fields

                    TextField(
                      controller: descriptionController,
                      obscureText: false, // Hide the entered text
                      decoration: const InputDecoration(
                        hintText: 'description',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    const SizedBox(
                        height: 16.0), // Add some space below the fields
                    ElevatedButton(
                      onPressed: () {
                        register();
                      },
                      child: const Text('Create Note'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
