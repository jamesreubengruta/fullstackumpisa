import 'package:flutter/material.dart';
import 'package:umpisa_flutter/modules/notes/presentation/note_create_screen.dart';

class NotesIntroScreen extends StatelessWidget {
  const NotesIntroScreen({super.key});
  static const routeName = '/note_intro';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(
          color: Colors.amber,
        ),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'No existing notes',
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 32.0), // Add some space below the header
              ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, NoteCreateScreen.routeName);
                },
                child: const Text('Create a new note'),
              ),
              const SizedBox(height: 16.0), // Add some space between buttons
            ],
          ),
        ),
      ),
    );
  }
}
