import 'package:fpdart/fpdart.dart';
import 'package:umpisa_flutter/core/errors/failure_api_exception.dart';
import 'package:umpisa_flutter/core/errors/failure_model.dart';
import 'package:umpisa_flutter/core/utils/typedef.dart';
import 'package:umpisa_flutter/modules/notes/data/datasources/notes_datasource.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';
import 'package:umpisa_flutter/modules/notes/domain/repository/notes_repo.dart';

class NotesRepoImpl extends NotesRepository {
  final NotesDataSource dataSource;

  const NotesRepoImpl(this.dataSource);

  @override
  ResulT<NotesModel> deleteNote({required String id}) async {
    try {
      final x = await dataSource.deleteNote(id: id);
      return Right(x);
    } on ApiServerException catch (e) {
      return Left(ApiFailure.fromException(e));
    }
  }

  @override
  ResulT<List<NotesModel>> getNotes({required String name}) async {
    try {
      final x = await dataSource.getNotes();
      return Right(x);
    } on ApiServerException catch (e) {
      return Left(ApiFailure.fromException(e));
    }
  }

  @override
  ResulT<NotesModel> updateNote(
      {required String id, required String title, required String note}) {
    throw UnimplementedError();
  }

  @override
  ResulT<NotesModel> addNote(
      {required String description,
      required String title,
      required String note}) async {
    try {
      final x = await dataSource.createNote(
          title: title, note: note, description: description);
      return Right(x);
    } on ApiServerException catch (e) {
      return Left(ApiFailure.fromException(e));
    }
  }
}
