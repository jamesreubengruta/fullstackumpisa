import 'dart:convert';

import 'package:dart_mappable/dart_mappable.dart';
import 'package:umpisa_flutter/core/utils/typedef.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';

part 'notes_dto.mapper.dart';

@MappableClass()
class NotesDTO extends NotesModel with NotesDTOMappable {
  NotesDTO(
      {required super.id,
      required super.title,
      required super.note,
      required super.description,
      required super.date});

  factory NotesDTO.fromJson(String json) =>
      NotesDTO.fromMap(jsonDecode(json) as DataMap);

  NotesDTO.fromMap(DataMap map)
      : this(
          id: map['id'].toString(),
          title: map['title'] ?? '',
          note: map['note'] ?? '',
          description: map['description'] ?? '',
          date: map['date'] ?? '',
        );
}

extension NotesDTOExtension on NotesModel {
  NotesDTO toModel() {
    return NotesDTO(
        id: id, title: title, note: note, description: description, date: date);
  }
}
