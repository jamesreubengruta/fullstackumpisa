// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'notes_dto.dart';

class NotesDTOMapper extends ClassMapperBase<NotesDTO> {
  NotesDTOMapper._();

  static NotesDTOMapper? _instance;
  static NotesDTOMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = NotesDTOMapper._());
    }
    return _instance!;
  }

  @override
  final String id = 'NotesDTO';

  static String _$id(NotesDTO v) => v.id;
  static const Field<NotesDTO, String> _f$id = Field('id', _$id);
  static String _$title(NotesDTO v) => v.title;
  static const Field<NotesDTO, String> _f$title = Field('title', _$title);
  static String _$note(NotesDTO v) => v.note;
  static const Field<NotesDTO, String> _f$note = Field('note', _$note);
  static String _$description(NotesDTO v) => v.description;
  static const Field<NotesDTO, String> _f$description =
      Field('description', _$description);
  static String _$date(NotesDTO v) => v.date;
  static const Field<NotesDTO, String> _f$date = Field('date', _$date);

  @override
  final MappableFields<NotesDTO> fields = const {
    #id: _f$id,
    #title: _f$title,
    #note: _f$note,
    #description: _f$description,
    #date: _f$date,
  };

  static NotesDTO _instantiate(DecodingData data) {
    return NotesDTO(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        note: data.dec(_f$note),
        description: data.dec(_f$description),
        date: data.dec(_f$date));
  }

  @override
  final Function instantiate = _instantiate;

  static NotesDTO fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<NotesDTO>(map);
  }

  static NotesDTO fromJson(String json) {
    return ensureInitialized().decodeJson<NotesDTO>(json);
  }
}

mixin NotesDTOMappable {
  String toJson() {
    return NotesDTOMapper.ensureInitialized()
        .encodeJson<NotesDTO>(this as NotesDTO);
  }

  Map<String, dynamic> toMap() {
    return NotesDTOMapper.ensureInitialized()
        .encodeMap<NotesDTO>(this as NotesDTO);
  }

  NotesDTOCopyWith<NotesDTO, NotesDTO, NotesDTO> get copyWith =>
      _NotesDTOCopyWithImpl(this as NotesDTO, $identity, $identity);
  @override
  String toString() {
    return NotesDTOMapper.ensureInitialized().stringifyValue(this as NotesDTO);
  }

  @override
  bool operator ==(Object other) {
    return NotesDTOMapper.ensureInitialized()
        .equalsValue(this as NotesDTO, other);
  }

  @override
  int get hashCode {
    return NotesDTOMapper.ensureInitialized().hashValue(this as NotesDTO);
  }
}

extension NotesDTOValueCopy<$R, $Out> on ObjectCopyWith<$R, NotesDTO, $Out> {
  NotesDTOCopyWith<$R, NotesDTO, $Out> get $asNotesDTO =>
      $base.as((v, t, t2) => _NotesDTOCopyWithImpl(v, t, t2));
}

abstract class NotesDTOCopyWith<$R, $In extends NotesDTO, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call(
      {String? id,
      String? title,
      String? note,
      String? description,
      String? date});
  NotesDTOCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _NotesDTOCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, NotesDTO, $Out>
    implements NotesDTOCopyWith<$R, NotesDTO, $Out> {
  _NotesDTOCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<NotesDTO> $mapper =
      NotesDTOMapper.ensureInitialized();
  @override
  $R call(
          {String? id,
          String? title,
          String? note,
          String? description,
          String? date}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (note != null) #note: note,
        if (description != null) #description: description,
        if (date != null) #date: date
      }));
  @override
  NotesDTO $make(CopyWithData data) => NotesDTO(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      note: data.get(#note, or: $value.note),
      description: data.get(#description, or: $value.description),
      date: data.get(#date, or: $value.date));

  @override
  NotesDTOCopyWith<$R2, NotesDTO, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _NotesDTOCopyWithImpl($value, $cast, t);
}
