import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:umpisa_flutter/core/errors/failure_api_exception.dart';
import 'package:umpisa_flutter/core/utils/date_helper.dart';
import 'package:umpisa_flutter/core/utils/secure_storage.dart';
import 'package:umpisa_flutter/modules/notes/data/dto/notes_dto.dart';

abstract class NotesDataSource {
  Future<List<NotesDTO>> getNotes();
  Future<NotesDTO> createNote({
    required String title,
    required String note,
    required String description,
  });
  Future<NotesDTO> deleteNote({required String id});
  Future<List<NotesDTO>> updateNote(
      {required String id,
      required String title,
      required String note,
      required String date,
      required String description});
}

class NodesDataSourceRemoteHttp extends NotesDataSource {
  final Dio dio;
  final PersistCookieJar cookieJar;
  final SecureStorageUtil storageUtil;
  String baseURL = dotenv.get('base_url');
  NodesDataSourceRemoteHttp(
      {required this.dio, required this.cookieJar, required this.storageUtil});

  @override
  Future<List<NotesDTO>> getNotes() async {
    const String url = '/notes';
    CookieManager manager = CookieManager(cookieJar);
    dio.interceptors.add(manager);

    try {
      final response = await dio.get(
        baseURL + url,
        options: Options(responseType: ResponseType.json),
      );

      debugPrint('data here${response.data}');
      final decoded = response.data;

      if (decoded is List) {
        List<NotesDTO> list = [];
        for (int x = 0; x < decoded.length; x++) {
          list.add(NotesDTO.fromMap(decoded[x]));
        }

        return list;
      } else if (decoded is Map) {
        return [NotesDTO.fromMap(decoded.cast())];
      }

      if (response.statusCode != 200 && response.statusCode != 201) {
        throw ApiServerException(
            message: response.data, statusCode: response.statusCode ?? 400);
      }

      return [];
    } on ApiServerException {
      rethrow;
    } catch (e) {
      throw const ApiServerException(message: 'code error', statusCode: 505);
    }
  }

  @override
  Future<NotesDTO> createNote(
      {required String title,
      required String note,
      required String description}) async {
    const String url = '/notes';
    final dateHelper = DateHelper();
    await dateHelper.initializeLocaleData(); // Initialize locale data
    final formattedDate = dateHelper.getFormattedDate();
    final Map<String, dynamic> queryParams = {
      'title': title,
      'note': note,
      'description': description,
      'date': formattedDate
    };

    CookieManager manager = CookieManager(cookieJar);
    dio.interceptors.add(manager);

    try {
      final response = await dio.post(
        baseURL + url,
        data: queryParams,
        options: Options(responseType: ResponseType.json),
      );

      final decoded = response.data;
      NotesDTO dto = NotesDTO.fromMap(decoded);

      if (response.statusCode != 200 && response.statusCode != 201) {
        throw ApiServerException(
            message: response.data, statusCode: response.statusCode ?? 400);
      }

      return dto;
    } on ApiServerException {
      rethrow;
    } catch (e) {
      debugPrint('createNote error $e');

      throw const ApiServerException(message: 'code error', statusCode: 505);
    }
  }

  @override
  Future<NotesDTO> deleteNote({required String id}) async {
    String url = '/notes/$id';

    CookieManager manager = CookieManager(cookieJar);
    dio.interceptors.add(manager);
    debugPrint(id + baseURL + url);
    try {
      final response = await dio.delete(
        baseURL + url,
        options: Options(responseType: ResponseType.json),
      );

      if (response.statusCode != 200 && response.statusCode != 201) {
        throw ApiServerException(
            message: response.data, statusCode: response.statusCode ?? 400);
      }

      final decoded = response.data;
      if (decoded is List) {
        List<NotesDTO> list = [];
        for (int x = 0; x < decoded.length; x++) {
          list.add(NotesDTO.fromMap(decoded[x]));
        }
        return list[0];
      } else {
        NotesDTO dto = NotesDTO.fromMap(decoded);
        return dto;
      }
    } on ApiServerException {
      rethrow;
    } catch (e) {
      debugPrint('delete Note error $e');

      throw const ApiServerException(message: 'code error', statusCode: 505);
    }
  }

  @override
  Future<List<NotesDTO>> updateNote(
      {required String id,
      required String title,
      required String note,
      required String date,
      required String description}) {
    throw UnimplementedError();
  }
}
