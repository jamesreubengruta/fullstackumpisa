import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:umpisa_flutter/core/presentation/widgets/back_header.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/login/bloc.dart';
import 'package:umpisa_flutter/modules/dashboard/presentation/dashboard.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});
  static const routeName = '/login';

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool loading = true;
  TextEditingController emailController = TextEditingController(text: '');
  TextEditingController passwordController = TextEditingController(text: '');

  @override
  void dispose() {
    super.dispose();
    emailController.dispose();
    passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    void login() {
      String email = emailController.text;
      String password = passwordController.text;
      context
          .read<AccountBloc>()
          .add(LoginAccountEvent(email: email, password: password));
    }

    return BlocConsumer<AccountBloc, AccountLoginState>(
      listener: (context, state) {
        if (state is LoginErrorState) {
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text(state.message)));
          loading = false;
        } else if (state is LoginSuccessState) {
          loading = false;
          ScaffoldMessenger.of(context)
              .showSnackBar(const SnackBar(content: Text('login success!')));

          Navigator.pushReplacementNamed(context, Dashboard.routeName);
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: CustomHeader(
            title: 'Login',
            onBackButtonPressed: () {
              Navigator.pop(context);
            },
          ),
          body: Container(
            constraints: const BoxConstraints.expand(),
            decoration: const BoxDecoration(
              color: Colors.amber,
            ),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextField(
                      controller: emailController,
                      decoration: const InputDecoration(
                        hintText: 'Email',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    const SizedBox(
                        height: 16.0), // Add some space between fields
                    TextField(
                      controller: passwordController,
                      obscureText: true, // Hide the entered text
                      decoration: const InputDecoration(
                        hintText: 'Password',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    const SizedBox(
                        height: 16.0), // Add some space below the fields
                    ElevatedButton(
                      onPressed: () {
                        login();
                      },
                      child: const Text('Login'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
