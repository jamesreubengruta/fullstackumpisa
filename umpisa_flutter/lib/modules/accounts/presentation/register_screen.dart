import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:umpisa_flutter/core/presentation/widgets/back_header.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/register/register_bloc.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/register/register_event.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/register/register_state.dart';
import 'package:umpisa_flutter/modules/accounts/presentation/login_screen.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});
  static const routeName = '/register';

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool loading = true;
  TextEditingController emailController = TextEditingController(text: '');
  TextEditingController passwordController = TextEditingController(text: '');
  TextEditingController confirmController = TextEditingController(text: '');
  TextEditingController nameController = TextEditingController(text: '');
  TextEditingController descriptionController = TextEditingController(text: '');

  @override
  void dispose() {
    super.dispose();
    emailController.dispose();
    passwordController.dispose();
    nameController.dispose();
    descriptionController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    void register() {
      String email = emailController.text;
      String password = passwordController.text;
      String name = nameController.text;
      String description = descriptionController.text;
      String confirmPassword = confirmController.text;

      if (password == confirmPassword) {
        context.read<RegisterBloc>().add(RegisterAccountEvent(
              email: email,
              password: password,
              name: name,
              description: description,
            ));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text("passwords do not match")));
      }
    }

    return BlocConsumer<RegisterBloc, AccountRegisterState>(
      listener: (context, state) {
        if (state is RegisterErrorState) {
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text(state.message)));
          loading = false;
        } else if (state is RegisterSuccessState) {
          loading = false;
          ScaffoldMessenger.of(context)
              .showSnackBar(const SnackBar(content: Text('Register success!')));

          Navigator.pushReplacementNamed(context, LoginScreen.routeName);
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: CustomHeader(
            title: 'Register',
            onBackButtonPressed: () {
              Navigator.pop(context);
            },
          ),
          body: Container(
            constraints: const BoxConstraints.expand(),
            decoration: const BoxDecoration(
              color: Colors.amber,
            ),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextField(
                      controller: nameController,
                      decoration: const InputDecoration(
                        hintText: 'Name',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    TextField(
                      controller: emailController,
                      decoration: const InputDecoration(
                        hintText: 'Email',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    const SizedBox(
                        height: 16.0), // Add some space between fields
                    TextField(
                      controller: passwordController,
                      obscureText: true, // Hide the entered text
                      decoration: const InputDecoration(
                        hintText: 'Password',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    TextField(
                      controller: confirmController,
                      obscureText: true, // Hide the entered text
                      decoration: const InputDecoration(
                        hintText: 'Confirm password',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    TextField(
                      controller: descriptionController,
                      obscureText: false, // Hide the entered text
                      decoration: const InputDecoration(
                        hintText: 'description',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    const SizedBox(
                        height: 16.0), // Add some space below the fields
                    ElevatedButton(
                      onPressed: () {
                        register();
                      },
                      child: const Text('Register'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
