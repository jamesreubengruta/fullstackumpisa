import 'package:flutter/material.dart';
import 'package:umpisa_flutter/core/utils/secure_storage.dart';
import 'package:umpisa_flutter/modules/accounts/presentation/splash_screen.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});
  static const routeName = '/profile';

  Future<void> logout(BuildContext context) async {
    // Clear stored cookie
    await SecureStorageUtil.putString('cookie', '');

    // Navigate back to login screen
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(
          color: Colors.amber,
        ),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'My Profile',
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 32.0), // Add some space below the header
              ElevatedButton(
                onPressed: () {
                  logout(context);
                  Navigator.pushReplacementNamed(
                      context, SplashScreen.routeName);
                },
                child: const Text('Logout'),
              ),
              const SizedBox(height: 16.0), // Add some space between buttons
            ],
          ),
        ),
      ),
    );
  }
}
