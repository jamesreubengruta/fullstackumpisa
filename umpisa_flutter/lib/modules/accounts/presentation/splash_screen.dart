import 'package:flutter/material.dart';
import 'package:umpisa_flutter/core/utils/secure_storage.dart';
import 'package:umpisa_flutter/modules/accounts/presentation/login_screen.dart';
import 'package:umpisa_flutter/modules/accounts/presentation/register_screen.dart';
import 'package:umpisa_flutter/modules/appboxo/presentation/appboxo_screen.dart';
import 'package:umpisa_flutter/modules/dashboard/presentation/dashboard.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});
  static const routeName = '/splash_screen';

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String?>(
      future: SecureStorageUtil.getString('cookie'),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        } else {
          if (snapshot.data != null && snapshot.data!.length > 5) {
            debugPrint(snapshot.data);
            Future.delayed(Duration.zero, () {
              Navigator.pushReplacementNamed(context, Dashboard.routeName);
            });
            return const SizedBox();
          } else {
            return Scaffold(
              body: Container(
                constraints: const BoxConstraints.expand(),
                decoration: const BoxDecoration(
                  color: Colors.amber,
                ),
                child: SafeArea(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'Umpisa Note App',
                        style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(
                          height: 32.0), // Add some space below the header
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(
                              context, RegisterScreen.routeName);
                        },
                        child: const Text('Register'),
                      ),
                      const SizedBox(
                          height: 16.0), // Add some space between buttons
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, LoginScreen.routeName);
                        },
                        child: const Text('Login'),
                      ),
                      const SizedBox(
                          height: 16.0), // Add some space between buttons
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, AppBoxoScreen.routeName);
                        },
                        child: const Text('Appboxo'),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
        }
      },
    );
  }
}
