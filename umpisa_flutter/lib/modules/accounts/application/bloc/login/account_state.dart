import 'package:equatable/equatable.dart';
import 'package:umpisa_flutter/modules/accounts/domain/models/account_model.dart';

sealed class AccountLoginState extends Equatable {
  const AccountLoginState();

  @override
  List<Object> get props => [];
}

final class LoginInitialState extends AccountLoginState {
  const LoginInitialState();
}

final class LogginInState extends AccountLoginState {
  const LogginInState();
}

final class LoginSuccessState extends AccountLoginState {
  const LoginSuccessState({required this.model});
  final AccountModel model;
  @override
  List<Object> get props => [model];
}

class LoginErrorState extends AccountLoginState {
  final String message;

  const LoginErrorState({required this.message});

  @override
  List<Object> get props => [message];
}
