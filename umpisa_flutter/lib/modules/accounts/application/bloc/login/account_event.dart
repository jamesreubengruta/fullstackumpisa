import 'package:equatable/equatable.dart';

sealed class AccountEvent extends Equatable {
  const AccountEvent();

  @override
  List<Object> get props => [];
}

class LoginAccountEvent extends AccountEvent {
  final String email;
  final String password;

  const LoginAccountEvent({required this.email, required this.password});
  @override
  List<Object> get props => [email, password];
}
