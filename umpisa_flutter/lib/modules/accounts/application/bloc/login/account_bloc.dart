import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/login/bloc.dart';
import 'package:umpisa_flutter/modules/accounts/domain/usecases/login_uc.dart';

class AccountBloc extends Bloc<AccountEvent, AccountLoginState> {
  AccountBloc({required LoginUC usecase})
      : _loginUC = usecase,
        super(const LoginInitialState()) {
    on<LoginAccountEvent>(_loginHandler);
  }
  final LoginUC _loginUC;

  Future<void> _loginHandler(
      LoginAccountEvent event, Emitter<AccountLoginState> emit) async {
    emit(const LogginInState());
    final result = await _loginUC(
        LoginNoteParams(email: event.email, password: event.password));
    result.fold(
        (failure) => emit(LoginErrorState(message: failure.errorMessage)),
        (data) => emit(LoginSuccessState(model: data)));
  }
}
