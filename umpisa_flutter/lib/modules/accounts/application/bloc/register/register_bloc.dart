import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/register/register_event.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/register/register_state.dart';
import 'package:umpisa_flutter/modules/accounts/domain/usecases/register_uc.dart';

class RegisterBloc extends Bloc<RegisterAccountEvent, AccountRegisterState> {
  RegisterBloc({required RegisterUC usecase})
      : _registerUC = usecase,
        super(const RegisterInitialState()) {
    on<RegisterAccountEvent>(_registerHandler);
  }
  final RegisterUC _registerUC;

  Future<void> _registerHandler(
      RegisterAccountEvent event, Emitter<AccountRegisterState> emit) async {
    emit(const RegisterInState());
    final result = await _registerUC(RegisterNoteParams(
        email: event.email,
        password: event.password,
        name: event.name,
        description: event.description));
    result.fold(
        (failure) => emit(RegisterErrorState(message: failure.errorMessage)),
        (data) => emit(RegisterSuccessState(model: data)));
  }
}
