import 'package:equatable/equatable.dart';
import 'package:umpisa_flutter/modules/accounts/domain/models/account_model.dart';

sealed class AccountRegisterState extends Equatable {
  const AccountRegisterState();

  @override
  List<Object> get props => [];
}

final class RegisterInitialState extends AccountRegisterState {
  const RegisterInitialState();
}

final class RegisterInState extends AccountRegisterState {
  const RegisterInState();
}

final class RegisterSuccessState extends AccountRegisterState {
  const RegisterSuccessState({required this.model});
  final AccountModel model;
  @override
  List<Object> get props => [model];
}

class RegisterErrorState extends AccountRegisterState {
  final String message;

  const RegisterErrorState({required this.message});

  @override
  List<Object> get props => [message];
}
