import 'package:equatable/equatable.dart';

sealed class AccountEvent extends Equatable {
  const AccountEvent();

  @override
  List<Object> get props => [];
}

class RegisterAccountEvent extends AccountEvent {
  final String email;
  final String password;
  final String name;
  final String description;

  const RegisterAccountEvent(
      {required this.email,
      required this.password,
      required this.name,
      required this.description});
  @override
  List<Object> get props => [email, password];
}
