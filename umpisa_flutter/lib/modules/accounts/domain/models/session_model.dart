import 'package:equatable/equatable.dart';

class SessionModel extends Equatable {
  final String sessionToken;
  final String refreshToken;
  final String sessionExpiry;
  const SessionModel(
      {required this.sessionToken,
      required this.refreshToken,
      required this.sessionExpiry});

  @override
  List<Object?> get props => [sessionToken, refreshToken, sessionExpiry];
}
