import 'package:equatable/equatable.dart';

class AccountModel extends Equatable {
  final num id;
  final String name;
  final String email;
  final String password;
  final String description;

  const AccountModel(
      {required this.id,
      required this.name,
      required this.email,
      required this.password,
      required this.description});

  const AccountModel.empty()
      : id = 0,
        name = '',
        email = '',
        password = '',
        description = '';

  @override
  List<Object?> get props => [id, name, email];
}
