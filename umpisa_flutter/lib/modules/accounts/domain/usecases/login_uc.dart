import 'package:email_validator/email_validator.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:umpisa_flutter/core/errors/invalid_email_format_exception.dart';
import 'package:umpisa_flutter/core/usecases/usecase.dart';
import 'package:umpisa_flutter/core/utils/typedef.dart';
import 'package:umpisa_flutter/modules/accounts/domain/models/account_model.dart';
import 'package:umpisa_flutter/modules/accounts/domain/repository/accounts_repo.dart';

class LoginUC extends UsecaseParams<AccountModel, LoginNoteParams> {
  final AccountsRepository _repo;

  LoginUC({required AccountsRepository provider}) : _repo = provider;

  @override
  ResulT<AccountModel> call(LoginNoteParams p) async {
    debugPrint("lols${p.email}${p.email}");
    if (!EmailValidator.validate(p.email)) {
      throw InvalidEmailFormatException();
    }

    return _repo.login(email: p.email, password: p.password);
  }
}

class LoginNoteParams extends Equatable {
  final String email;
  final String password;

  const LoginNoteParams({required this.email, required this.password});

  @override
  List<Object?> get props => [email, password];
}
