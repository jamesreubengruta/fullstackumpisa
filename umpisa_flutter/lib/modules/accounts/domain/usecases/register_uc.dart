import 'package:email_validator/email_validator.dart';
import 'package:equatable/equatable.dart';
import 'package:umpisa_flutter/core/errors/invalid_email_format_exception.dart';
import 'package:umpisa_flutter/core/usecases/usecase.dart';
import 'package:umpisa_flutter/core/utils/typedef.dart';
import 'package:umpisa_flutter/modules/accounts/domain/models/account_model.dart';
import 'package:umpisa_flutter/modules/accounts/domain/repository/accounts_repo.dart';

class RegisterUC extends UsecaseParams<AccountModel, RegisterNoteParams> {
  final AccountsRepository _repo;

  RegisterUC({required AccountsRepository provider}) : _repo = provider;

  @override
  ResulT<AccountModel> call(RegisterNoteParams p) async {
    if (!EmailValidator.validate(p.email)) {
      throw InvalidEmailFormatException();
    }

    return _repo.register(
        email: p.email,
        password: p.password,
        description: p.description,
        name: p.name);
  }
}

class RegisterNoteParams extends Equatable {
  final String email;
  final String password;
  final String name;
  final String description;

  const RegisterNoteParams(
      {required this.email,
      required this.password,
      required this.name,
      required this.description});

  @override
  List<Object?> get props => [email, password, name];
}
