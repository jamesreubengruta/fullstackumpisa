import 'package:umpisa_flutter/core/utils/typedef.dart';
import 'package:umpisa_flutter/modules/accounts/domain/models/account_model.dart';

abstract class AccountsRepository {
  const AccountsRepository();

  ResulT<AccountModel> login({required String email, required String password});
  ResulT<AccountModel> register(
      {required String email,
      required String password,
      required String name,
      required String description});
}
