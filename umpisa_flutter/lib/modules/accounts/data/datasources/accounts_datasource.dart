import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:umpisa_flutter/core/errors/failure_api_exception.dart';
import 'package:umpisa_flutter/core/utils/secure_storage.dart';
import 'package:umpisa_flutter/modules/accounts/data/dto/accounts_dto.dart';

abstract class AccountsDataSource {
  Future<AccountsDTO> login({required String email, required String password});
  Future<AccountsDTO> register(
      {required String email,
      required String password,
      required String name,
      required String description});
}

class AccountsDataSourceRemoteHttp implements AccountsDataSource {
  final Dio dio;
  final SecureStorageUtil secureStorageUtil;
  final PersistCookieJar cookieJar;
  String baseURL = dotenv.get('base_url');
  AccountsDataSourceRemoteHttp(
      {required this.dio,
      required this.secureStorageUtil,
      required this.cookieJar});

  @override
  Future<AccountsDTO> login(
      {required String email, required String password}) async {
    String apiURL = '/auth/login';
    final Map<String, dynamic> queryParams = {
      'email': email,
      'password': password,
    };
    CookieManager manager = CookieManager(cookieJar);
    dio.interceptors.add(manager);

    try {
      final response = await dio.post(
        baseURL + apiURL,
        data: queryParams,
        options: Options(responseType: ResponseType.json),
      );

      final decoded = response.data;
      AccountsDTO dto = AccountsDTO.fromMap(decoded);

      if (response.statusCode != 200 && response.statusCode != 201) {
        throw ApiServerException(
            message: response.data, statusCode: response.statusCode ?? 400);
      } else {
        final cookie =
            await cookieJar.loadForRequest(Uri.parse(baseURL + apiURL));
        debugPrint('cookies kayo jan:     ${cookie[0]}');
        await SecureStorageUtil.putString('cookie', '${cookie[0]}');
      }

      return dto;
    } on ApiServerException {
      //mandatory so that api failure wont go try catch level error.

      rethrow;
    } catch (e) {
      debugPrint(' $baseURL+$apiURL $e');
      throw const ApiServerException(message: 'code error', statusCode: 505);
    }
  }

  @override
  Future<AccountsDTO> register(
      {required String email,
      required String password,
      required String name,
      required String description}) async {
    String apiURL = '/auth/signup';
    final Map<String, dynamic> queryParams = {
      'password': password,
      'email': email,
      'name': name,
      'description': description,
    };
    debugPrint(baseURL + apiURL);
    try {
      final response = await dio.post(
        baseURL + apiURL,
        data: queryParams,
        options: Options(responseType: ResponseType.json),
      );

      final decoded = response.data;
      AccountsDTO dto = AccountsDTO.fromMap(decoded);

      if (response.statusCode != 200 && response.statusCode != 201) {
        throw ApiServerException(
            message: response.data, statusCode: response.statusCode ?? 400);
      }

      return dto;
    } on ApiServerException {
      rethrow;
    } catch (e) {
      debugPrint(e.toString());
      throw const ApiServerException(message: 'code error', statusCode: 505);
    }
  }
}
