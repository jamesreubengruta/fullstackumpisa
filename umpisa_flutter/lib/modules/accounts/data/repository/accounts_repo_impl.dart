import 'package:fpdart/fpdart.dart';
import 'package:umpisa_flutter/core/core.dart';
import 'package:umpisa_flutter/modules/accounts/data/datasources/accounts_datasource.dart';
import 'package:umpisa_flutter/modules/accounts/domain/models/account_model.dart';
import 'package:umpisa_flutter/modules/accounts/domain/repository/accounts_repo.dart';

class AccountsRepoImp extends AccountsRepository {
  final AccountsDataSource dataSource;

  const AccountsRepoImp(this.dataSource);

  @override
  ResulT<AccountModel> login(
      {required String email, required String password}) async {
    try {
      final x = await dataSource.login(email: email, password: password);
      return Right(x);
    } on ApiServerException catch (e) {
      return Left(ApiFailure.fromException(e));
    }
  }

  @override
  ResulT<AccountModel> register(
      {required String email,
      required String password,
      required String name,
      required String description}) async {
    try {
      final x = await dataSource.register(
          email: email,
          password: password,
          name: name,
          description: description);
      return Right(x);
    } on ApiServerException catch (e) {
      return Left(ApiFailure.fromException(e));
    }
  }
}
