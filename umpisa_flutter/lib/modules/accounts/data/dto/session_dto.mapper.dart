// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'session_dto.dart';

class SessionDTOMapper extends ClassMapperBase<SessionDTO> {
  SessionDTOMapper._();

  static SessionDTOMapper? _instance;
  static SessionDTOMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = SessionDTOMapper._());
    }
    return _instance!;
  }

  @override
  final String id = 'SessionDTO';

  static String _$sessionToken(SessionDTO v) => v.sessionToken;
  static const Field<SessionDTO, String> _f$sessionToken =
      Field('sessionToken', _$sessionToken);
  static String _$refreshToken(SessionDTO v) => v.refreshToken;
  static const Field<SessionDTO, String> _f$refreshToken =
      Field('refreshToken', _$refreshToken);
  static String _$sessionExpiry(SessionDTO v) => v.sessionExpiry;
  static const Field<SessionDTO, String> _f$sessionExpiry =
      Field('sessionExpiry', _$sessionExpiry);

  @override
  final MappableFields<SessionDTO> fields = const {
    #sessionToken: _f$sessionToken,
    #refreshToken: _f$refreshToken,
    #sessionExpiry: _f$sessionExpiry,
  };

  static SessionDTO _instantiate(DecodingData data) {
    return SessionDTO(
        sessionToken: data.dec(_f$sessionToken),
        refreshToken: data.dec(_f$refreshToken),
        sessionExpiry: data.dec(_f$sessionExpiry));
  }

  @override
  final Function instantiate = _instantiate;

  static SessionDTO fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<SessionDTO>(map);
  }

  static SessionDTO fromJson(String json) {
    return ensureInitialized().decodeJson<SessionDTO>(json);
  }
}

mixin SessionDTOMappable {
  String toJson() {
    return SessionDTOMapper.ensureInitialized()
        .encodeJson<SessionDTO>(this as SessionDTO);
  }

  Map<String, dynamic> toMap() {
    return SessionDTOMapper.ensureInitialized()
        .encodeMap<SessionDTO>(this as SessionDTO);
  }

  SessionDTOCopyWith<SessionDTO, SessionDTO, SessionDTO> get copyWith =>
      _SessionDTOCopyWithImpl(this as SessionDTO, $identity, $identity);
  @override
  String toString() {
    return SessionDTOMapper.ensureInitialized()
        .stringifyValue(this as SessionDTO);
  }

  @override
  bool operator ==(Object other) {
    return SessionDTOMapper.ensureInitialized()
        .equalsValue(this as SessionDTO, other);
  }

  @override
  int get hashCode {
    return SessionDTOMapper.ensureInitialized().hashValue(this as SessionDTO);
  }
}

extension SessionDTOValueCopy<$R, $Out>
    on ObjectCopyWith<$R, SessionDTO, $Out> {
  SessionDTOCopyWith<$R, SessionDTO, $Out> get $asSessionDTO =>
      $base.as((v, t, t2) => _SessionDTOCopyWithImpl(v, t, t2));
}

abstract class SessionDTOCopyWith<$R, $In extends SessionDTO, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? sessionToken, String? refreshToken, String? sessionExpiry});
  SessionDTOCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _SessionDTOCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, SessionDTO, $Out>
    implements SessionDTOCopyWith<$R, SessionDTO, $Out> {
  _SessionDTOCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<SessionDTO> $mapper =
      SessionDTOMapper.ensureInitialized();
  @override
  $R call(
          {String? sessionToken,
          String? refreshToken,
          String? sessionExpiry}) =>
      $apply(FieldCopyWithData({
        if (sessionToken != null) #sessionToken: sessionToken,
        if (refreshToken != null) #refreshToken: refreshToken,
        if (sessionExpiry != null) #sessionExpiry: sessionExpiry
      }));
  @override
  SessionDTO $make(CopyWithData data) => SessionDTO(
      sessionToken: data.get(#sessionToken, or: $value.sessionToken),
      refreshToken: data.get(#refreshToken, or: $value.refreshToken),
      sessionExpiry: data.get(#sessionExpiry, or: $value.sessionExpiry));

  @override
  SessionDTOCopyWith<$R2, SessionDTO, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _SessionDTOCopyWithImpl($value, $cast, t);
}
