// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'accounts_dto.dart';

class AccountsDTOMapper extends ClassMapperBase<AccountsDTO> {
  AccountsDTOMapper._();

  static AccountsDTOMapper? _instance;
  static AccountsDTOMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = AccountsDTOMapper._());
    }
    return _instance!;
  }

  @override
  final String id = 'AccountsDTO';

  static num _$id(AccountsDTO v) => v.id;
  static const Field<AccountsDTO, num> _f$id =
      Field('id', _$id, opt: true, def: 0);
  static String _$name(AccountsDTO v) => v.name;
  static const Field<AccountsDTO, String> _f$name =
      Field('name', _$name, opt: true, def: '');
  static String _$email(AccountsDTO v) => v.email;
  static const Field<AccountsDTO, String> _f$email =
      Field('email', _$email, opt: true, def: '');
  static String _$password(AccountsDTO v) => v.password;
  static const Field<AccountsDTO, String> _f$password =
      Field('password', _$password, opt: true, def: '');
  static String _$description(AccountsDTO v) => v.description;
  static const Field<AccountsDTO, String> _f$description =
      Field('description', _$description, opt: true, def: '');

  @override
  final MappableFields<AccountsDTO> fields = const {
    #id: _f$id,
    #name: _f$name,
    #email: _f$email,
    #password: _f$password,
    #description: _f$description,
  };

  static AccountsDTO _instantiate(DecodingData data) {
    return AccountsDTO(
        id: data.dec(_f$id),
        name: data.dec(_f$name),
        email: data.dec(_f$email),
        password: data.dec(_f$password),
        description: data.dec(_f$description));
  }

  @override
  final Function instantiate = _instantiate;

  static AccountsDTO fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<AccountsDTO>(map);
  }

  static AccountsDTO fromJson(String json) {
    return ensureInitialized().decodeJson<AccountsDTO>(json);
  }
}

mixin AccountsDTOMappable {
  String toJson() {
    return AccountsDTOMapper.ensureInitialized()
        .encodeJson<AccountsDTO>(this as AccountsDTO);
  }

  Map<String, dynamic> toMap() {
    return AccountsDTOMapper.ensureInitialized()
        .encodeMap<AccountsDTO>(this as AccountsDTO);
  }

  AccountsDTOCopyWith<AccountsDTO, AccountsDTO, AccountsDTO> get copyWith =>
      _AccountsDTOCopyWithImpl(this as AccountsDTO, $identity, $identity);
  @override
  String toString() {
    return AccountsDTOMapper.ensureInitialized()
        .stringifyValue(this as AccountsDTO);
  }

  @override
  bool operator ==(Object other) {
    return AccountsDTOMapper.ensureInitialized()
        .equalsValue(this as AccountsDTO, other);
  }

  @override
  int get hashCode {
    return AccountsDTOMapper.ensureInitialized().hashValue(this as AccountsDTO);
  }
}

extension AccountsDTOValueCopy<$R, $Out>
    on ObjectCopyWith<$R, AccountsDTO, $Out> {
  AccountsDTOCopyWith<$R, AccountsDTO, $Out> get $asAccountsDTO =>
      $base.as((v, t, t2) => _AccountsDTOCopyWithImpl(v, t, t2));
}

abstract class AccountsDTOCopyWith<$R, $In extends AccountsDTO, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call(
      {num? id,
      String? name,
      String? email,
      String? password,
      String? description});
  AccountsDTOCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _AccountsDTOCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, AccountsDTO, $Out>
    implements AccountsDTOCopyWith<$R, AccountsDTO, $Out> {
  _AccountsDTOCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<AccountsDTO> $mapper =
      AccountsDTOMapper.ensureInitialized();
  @override
  $R call(
          {num? id,
          String? name,
          String? email,
          String? password,
          String? description}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (name != null) #name: name,
        if (email != null) #email: email,
        if (password != null) #password: password,
        if (description != null) #description: description
      }));
  @override
  AccountsDTO $make(CopyWithData data) => AccountsDTO(
      id: data.get(#id, or: $value.id),
      name: data.get(#name, or: $value.name),
      email: data.get(#email, or: $value.email),
      password: data.get(#password, or: $value.password),
      description: data.get(#description, or: $value.description));

  @override
  AccountsDTOCopyWith<$R2, AccountsDTO, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _AccountsDTOCopyWithImpl($value, $cast, t);
}
