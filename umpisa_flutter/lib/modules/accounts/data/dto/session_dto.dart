import 'package:dart_mappable/dart_mappable.dart';
import 'package:umpisa_flutter/modules/accounts/domain/models/session_model.dart';

part 'package:umpisa_flutter/modules/accounts/data/dto/session_dto.mapper.dart';

@MappableClass()
class SessionDTO extends SessionModel with SessionDTOMappable {
  SessionDTO(
      {required super.sessionToken,
      required super.refreshToken,
      required super.sessionExpiry});
}
