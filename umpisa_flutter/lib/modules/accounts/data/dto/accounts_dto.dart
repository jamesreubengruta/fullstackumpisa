import 'dart:convert';

import 'package:dart_mappable/dart_mappable.dart';
import 'package:umpisa_flutter/core/core.dart';
import 'package:umpisa_flutter/modules/accounts/domain/models/account_model.dart';

part 'package:umpisa_flutter/modules/accounts/data/dto/accounts_dto.mapper.dart';

@MappableClass()
class AccountsDTO extends AccountModel with AccountsDTOMappable {
  const AccountsDTO({
    @MappableField(key: 'id') super.id = 0,
    @MappableField(key: 'name') super.name = '',
    @MappableField(key: 'email') super.email = '',
    @MappableField(key: 'password') super.password = '',
    @MappableField(key: 'description') super.description = '',
  });

  factory AccountsDTO.fromJson(String json) =>
      AccountsDTO.fromMap(jsonDecode(json) as DataMap);

  AccountsDTO.fromMap(DataMap map)
      : this(
          id: map['id'] ?? 0,
          name: map['name'] ?? '',
          email: map['email'] ?? '',
          password: map['password'] ?? '',
          description: map['description'] ?? '',
        );
}

extension AccountsDTOExtension on AccountModel {
  AccountsDTO toModel() {
    return AccountsDTO(
      id: id,
      name: name,
      email: email,
      password: password,
      description: description,
    );
  }
}
