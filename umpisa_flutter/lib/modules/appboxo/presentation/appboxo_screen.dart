import 'dart:convert';

import 'package:appboxo_sdk/appboxo_sdk.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

class AppBoxoScreen extends StatefulWidget {
  const AppBoxoScreen({super.key});
  static const routeName = '/appboxo_screen';

  @override
  State<AppBoxoScreen> createState() => _AppBoxoScreenState();
}

class _AppBoxoScreenState extends State<AppBoxoScreen> {
  Future<void> Function()? subscription;

  @override
  void initState() {
    Appboxo.setConfig("243912", multitaskMode: false, sandboxMode: false);
    //'multitaskMode' works only on Android. By default 'true', each miniapp appears as a task in the Recents screen.
    Appboxo.customEvents().listen((CustomEvent event) {
      debugPrint(event.toJson().toString());
      Fluttertoast.showToast(
        msg: event.payload["message"] ?? event.type,
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0,
      );
      Appboxo.sendEvent(event);
    });
    Appboxo.paymentEvents().listen((event) {
      event.status = "success";
      Appboxo.sendPaymentEvent(event);
    });
    Appboxo.getMiniapps();
    subscription = Appboxo.lifecycleHooksListener(
      onAuth: (appId) {
        http
            .get(Uri.parse(
                'https://demo-hostapp.appboxo.com/api/get_auth_code/'))
            .then((response) {
          if (response.statusCode >= 400) {
            debugPrint('Error fetching auth code: ${response.body}');
            Appboxo.setAuthCode(appId, "");
          } else {
            // debugPrint(' fetching auth code: ${response.body}');
            // Map<String, dynamic> rv = jsonDecode(response.body.toString());
            // String authCode = rv['auth_code'];
            // debugPrint(appId + authCode);
            // //Appboxo.setAuthCode(appId, "tNCYV57xV03Ds3ar63oQtddQxUxCRY");
            // Appboxo.setAuthCode(appId, authCode);
            try {
              debugPrint('Fetching auth code: ${response.body}');
              Map<String, dynamic> rv = jsonDecode(response.body.toString());
              String authCode = rv['auth_code'];
              debugPrint('$appId $authCode');
              Appboxo.setAuthCode(appId, authCode);
            } catch (e) {
              // If JSON decoding fails, log the error and the response body
              debugPrint('Failed to decode JSON response: ${response.body}');
              debugPrint('Error: $e');
              Appboxo.setAuthCode(appId, "");
            }
          }
        }).catchError((error) {
          debugPrint('HTTP request failed: $error');
          //Appboxo.setAuthCode(appId, "");
        });
      },
      onLaunch: (appId) {
        debugPrint(appId);
        debugPrint('onLaunch');
      },
      onResume: (appId) {
        debugPrint(appId);
        debugPrint('onResume');
      },
      onPause: (appId) {
        debugPrint(appId);
        debugPrint('onPause');
      },
      onClose: (appId) {
        debugPrint(appId);
        debugPrint('onClose');
      },
      onError: (appId, error) {
        debugPrint(appId);
        debugPrint(error);
        debugPrint('onError');
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(
          color: Colors.amber,
        ),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {
                  Appboxo.openMiniapp("app16973", data: {
                    'title': 'Title',
                  }, extraUrlParams: {}, colors: {
                    'primary_color': '#000000',
                    'secondary_color': '#0000FF',
                    'tertiary_color': '#00FF00'
                  });
                },
                child: const Text(
                  'Run miniapp',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    subscription!();
    super.dispose();
  }
}
