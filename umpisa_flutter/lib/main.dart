import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:umpisa_flutter/core/presentation/providers/dashboard_controller.dart';
import 'package:umpisa_flutter/core/presentation/tools/router.dart';
import 'package:umpisa_flutter/core/utils/injector.dart';
import 'package:umpisa_flutter/generated/l10n.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/login/account_bloc.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/register/register_bloc.dart';
import 'package:umpisa_flutter/modules/accounts/presentation/login_screen.dart';
import 'package:umpisa_flutter/modules/accounts/presentation/register_screen.dart';
import 'package:umpisa_flutter/modules/notes/application/bloc/note_bloc.dart';
import 'package:umpisa_flutter/modules/notes/presentation/note_create_screen.dart';
import 'package:umpisa_flutter/modules/notes/presentation/note_detail.dart';
import 'package:umpisa_flutter/modules/notes/presentation/note_list_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dependencyInjector();

  runApp(const AppWidget());
}

class AppWidget extends StatelessWidget {
  const AppWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AccountBloc>(
          create: (BuildContext context) => sl<AccountBloc>(),
          child: const LoginScreen(),
        ),
        BlocProvider<RegisterBloc>(
          create: (BuildContext context) => sl<RegisterBloc>(),
          child: const RegisterScreen(),
        ),
        BlocProvider<NoteBloc>(
          create: (BuildContext context) => sl<NoteBloc>(),
          child: const NoteCreateScreen(),
        ),
        BlocProvider<NoteBloc>(
          create: (BuildContext context) => sl<NoteBloc>(),
          child: const NoteListScreen(),
        ),
        BlocProvider<NoteBloc>(
          create: (BuildContext context) => sl<NoteBloc>(),
          child: const NoteDetailScreen(),
        ),
      ],
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => DashboardController()),
        ],
        child: ScreenUtilInit(
          designSize: const Size(430, 932),
          splitScreenMode: true,
          minTextAdapt: true,
          builder: (_, child) {
            return MaterialApp(
              title: 'Umpisa Note APP',
              theme: ThemeData(
                useMaterial3: true,
                visualDensity: VisualDensity.adaptivePlatformDensity,
                appBarTheme: const AppBarTheme(color: Colors.transparent),
                colorScheme: ColorScheme.fromSeed(seedColor: Colors.white),
              ),
              initialRoute: '/',
              onGenerateRoute: generateRoute,
              localizationsDelegates: const [
                S.delegate,
                // GlobalMaterialLocalizations.delegate,
                // GlobalWidgetsLocalizations.delegate,
                // GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
            );
          },
        ),
      ),
    ); //added by extension
  }
}
