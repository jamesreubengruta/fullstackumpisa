import 'package:flutter/material.dart';

class NoteItemWidget extends StatelessWidget {
  final String id;
  final String title;
  final String note;
  final String description;
  final String date;

  const NoteItemWidget({
    super.key,
    required this.id,
    required this.title,
    required this.note,
    required this.description,
    required this.date,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'ID: $id',
              maxLines: 2,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
            const SizedBox(height: 8),
            Text(
              'Title: $title',
              maxLines: 2,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              'Note: $note',
              maxLines: 5,
              overflow: TextOverflow.ellipsis,
            ),
            Text('Description: $description'),
            const SizedBox(height: 8),
            Text(
              date,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
