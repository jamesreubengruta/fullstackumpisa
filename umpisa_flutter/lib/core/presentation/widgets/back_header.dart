import 'package:flutter/material.dart';

class CustomHeader extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final VoidCallback? onBackButtonPressed;
  final bool showBackbutton;
  const CustomHeader({
    required this.title,
    this.onBackButtonPressed,
    this.showBackbutton = true,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: Visibility(
        visible: showBackbutton,
        child: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: onBackButtonPressed ?? () => Navigator.of(context),
        ),
      ),
      title: Text(
        title,
        style: const TextStyle(
          fontSize: 24.0,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
