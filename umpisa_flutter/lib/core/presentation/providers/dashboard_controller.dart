import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:umpisa_flutter/core/presentation/providers/tab_navigator.dart';
import 'package:umpisa_flutter/core/presentation/ui/persistent_view.dart';
import 'package:umpisa_flutter/modules/accounts/presentation/profile_screen.dart';
import 'package:umpisa_flutter/modules/notes/presentation/note_list_screen.dart';

class DashboardController extends ChangeNotifier {
  List<int> _indexHistory = [0];

  final List<Widget> _screens = [
    ChangeNotifierProvider(
      create: (_) => TabNavigator(
        TabItem(child: const NoteListScreen()),
      ),
      child: const PersistentView(),
    ),
    ChangeNotifierProvider(
      create: (_) => TabNavigator(
        TabItem(child: const ProfileScreen()),
      ),
      child: const PersistentView(),
    ),
  ];

  List<Widget> get screens => _screens;
  int _currentIndex = 0;
  int get currentIndex => _currentIndex;

  void changeIndex(int index) {
    if (_currentIndex == index) return;
    _currentIndex = index;
    _indexHistory.add(index);
    notifyListeners();
  }

  void goBack() {
    if (_indexHistory.length == 1) return;
    _indexHistory.removeLast();
    _currentIndex = _indexHistory.last;
    notifyListeners();
  }

  void resetIndex() {
    _indexHistory = [0];
    _currentIndex = 0;
    notifyListeners();
  }
}
