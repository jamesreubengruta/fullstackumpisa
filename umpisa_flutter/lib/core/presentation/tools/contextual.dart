import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:umpisa_flutter/core/presentation/providers/tab_navigator.dart';

extension Contextual on BuildContext {
  ThemeData get theme => Theme.of(this);

  TabNavigator get tabNavigator => read<TabNavigator>();
  void pop() => tabNavigator.pop();
  void push(Widget page) => tabNavigator.push(TabItem(child: page));
}
