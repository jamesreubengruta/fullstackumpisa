import 'package:flutter/material.dart';
import 'package:umpisa_flutter/core/presentation/ui/wip.dart';
import 'package:umpisa_flutter/modules/accounts/presentation/login_screen.dart';
import 'package:umpisa_flutter/modules/accounts/presentation/profile_screen.dart';
import 'package:umpisa_flutter/modules/accounts/presentation/register_screen.dart';
import 'package:umpisa_flutter/modules/accounts/presentation/splash_screen.dart';
import 'package:umpisa_flutter/modules/appboxo/presentation/appboxo_screen.dart';
import 'package:umpisa_flutter/modules/dashboard/presentation/dashboard.dart';
import 'package:umpisa_flutter/modules/notes/presentation/note_create_screen.dart';
import 'package:umpisa_flutter/modules/notes/presentation/note_detail.dart';
import 'package:umpisa_flutter/modules/notes/presentation/note_intro_screen.dart';
import 'package:umpisa_flutter/modules/notes/presentation/note_list_screen.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case WIP.routeName:
      return _pageBuilder(
        (_) => const WIP(),
        settings: settings,
      );
    case RegisterScreen.routeName:
      return _pageBuilder(
        (_) => const RegisterScreen(),
        settings: settings,
      );
    case NotesIntroScreen.routeName:
      return _pageBuilder(
        (_) => const NotesIntroScreen(),
        settings: settings,
      );
    case NoteListScreen.routeName:
      return _pageBuilder(
        (_) => const NoteListScreen(),
        settings: settings,
      );
    case LoginScreen.routeName:
      return _pageBuilder(
        (_) => const LoginScreen(),
        settings: settings,
      );
    case NoteCreateScreen.routeName:
      return _pageBuilder(
        (_) => const NoteCreateScreen(),
        settings: settings,
      );
    case ProfileScreen.routeName:
      return _pageBuilder(
        (_) => const ProfileScreen(),
        settings: settings,
      );

    case Dashboard.routeName:
      return _pageBuilder(
        (_) => const Dashboard(),
        settings: settings,
      );
    case AppBoxoScreen.routeName:
      return _pageBuilder(
        (_) => const AppBoxoScreen(),
        settings: settings,
      );
    case NoteDetailScreen.routeName:
      return _pageBuilder(
        (_) => const NoteDetailScreen(),
        settings: settings,
      );

    default:
      return _pageBuilder((p0) => const SplashScreen(), settings: settings);
  }
}

PageRouteBuilder<dynamic> _pageBuilder(
  Widget Function(BuildContext) page, {
  required RouteSettings settings,
}) {
  return PageRouteBuilder(
    settings: settings,
    transitionsBuilder: (_, animation, __, child) => FadeTransition(
      opacity: animation,
      child: child,
    ),
    pageBuilder: (context, _, __) => page(context),
  );
}
