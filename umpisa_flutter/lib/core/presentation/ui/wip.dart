import 'package:flutter/material.dart';

class WIP extends StatelessWidget {
  const WIP({super.key});
  static const routeName = '/wip';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(
          color: Colors.amber,
        ),
        child: const SafeArea(
          child: Column(
            children: [
              Text('WIP'),
            ],
          ),
        ),
      ),
    );
  }
}
