import 'package:umpisa_flutter/core/utils/typedef.dart';

abstract class UsecaseParams<Type, Params> {
  const UsecaseParams();
  ResulT<Type> call(Params p);
}

abstract class Usecase<Type> {
  const Usecase();
  ResulT<Type> call();
}
