import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SecureStorageUtil {
  SecureStorageUtil();
  SecureStorageUtil._();

  static SecureStorageUtil? _secureStorageUtil;
  //mobile -FlutterSecureStorage
  static FlutterSecureStorage? _storage;
  //web -SharedPreferences
  static SharedPreferences? _preferences;

  Future<SecureStorageUtil> getInstance(bool isWeb) async {
    if (_secureStorageUtil == null) {
      final SecureStorageUtil secureStorage = SecureStorageUtil._();

      await secureStorage._init(isWeb);

      _secureStorageUtil = secureStorage;
    }

    return _secureStorageUtil!;
  }

  Future<void> _init(bool isWeb) async {
    if (!isWeb) {
      _storage = const FlutterSecureStorage();
    } else {
      _preferences = await SharedPreferences.getInstance();
    }
  }

  static Future<String?> getString(String key) async {
    if (_preferences == null) {
      return await _storage?.read(key: key);
    } else {
      return _preferences?.getString(key);
    }
  }

  static Future<void> putString(String key, String value) async {
    if (_preferences == null) {
      await _storage?.write(key: key, value: value);
    } else {
      await _preferences?.setString(key, value);
    }
  }
}
