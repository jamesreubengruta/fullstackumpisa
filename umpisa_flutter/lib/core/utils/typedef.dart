import 'package:fpdart/fpdart.dart';
import 'package:umpisa_flutter/core/errors/failure_model.dart';

typedef ResulT<T> = Future<Either<Failure, T>>;
typedef ResultV<T> = Future<void>;
typedef DataMap = Map<String, dynamic>;
