import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';

class CookieManager {
  static final CookieManager _instance = CookieManager._internal();

  late Dio _dio;
  late CookieJar _cookieJar;

  factory CookieManager(CookieJar cookieJar) {
    return _instance;
  }

  CookieManager._internal() {
    _cookieJar = CookieJar();
    _dio = Dio();
    _dio.interceptors.add(CookieManager(_cookieJar) as Interceptor);
  }

  void saveCookies(List<Cookie> cookies) {
    _cookieJar.saveFromResponse(Uri.parse('https://example.com'), cookies);
  }

  Future<List<Cookie>> loadCookies() async {
    return await _cookieJar.loadForRequest(Uri.parse('https://example.com'));
  }

  Dio getDioInstance() {
    return _dio;
  }

  Future<bool> hasCookies() {
    return _cookieJar
        .loadForRequest(Uri.parse('https://example.com'))
        .then((cookies) => cookies.isNotEmpty);
  }
}
