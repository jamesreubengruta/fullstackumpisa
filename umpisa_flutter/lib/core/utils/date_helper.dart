import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

class DateHelper {
  // Private constructor
  DateHelper._();

  // Singleton instance
  static final DateHelper _instance = DateHelper._();

  // Factory method to access the singleton instance
  factory DateHelper() => _instance;

  // Method to initialize locale data
  Future<void> initializeLocaleData() async {
    await initializeDateFormatting(); // Initialize locale data
  }

  // Method to get today's date in the desired format
  String getFormattedDate() {
    final now = DateTime.now();
    final formatter = DateFormat('dd-MM-yyyy');
    return formatter.format(now);
  }
}
