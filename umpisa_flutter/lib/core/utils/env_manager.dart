import 'package:flutter_dotenv/flutter_dotenv.dart';

class EnvManager {
  static final EnvManager _instance = EnvManager._internal();

  factory EnvManager() {
    return _instance;
  }

  EnvManager._internal();

  Future<void> load() async {
    await dotenv.load(fileName: './.env');
  }

  String get(String key, {String? fallback}) {
    return dotenv.get(key, fallback: fallback);
  }

  String? maybeGet(String key, {String? fallback}) {
    return dotenv.maybeGet(key, fallback: fallback);
  }
}
