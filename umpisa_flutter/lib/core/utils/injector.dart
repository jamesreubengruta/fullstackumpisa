import 'dart:io';

import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:path_provider/path_provider.dart';
import 'package:umpisa_flutter/core/utils/env_manager.dart';
import 'package:umpisa_flutter/core/utils/secure_storage.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/login/account_bloc.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/register/register_bloc.dart';
import 'package:umpisa_flutter/modules/accounts/data/datasources/accounts_datasource.dart';
import 'package:umpisa_flutter/modules/accounts/data/repository/accounts_repo_impl.dart';
import 'package:umpisa_flutter/modules/accounts/domain/repository/accounts_repo.dart';
import 'package:umpisa_flutter/modules/accounts/domain/usecases/login_uc.dart';
import 'package:umpisa_flutter/modules/accounts/domain/usecases/register_uc.dart';
import 'package:umpisa_flutter/modules/notes/application/bloc/note_bloc.dart';
import 'package:umpisa_flutter/modules/notes/data/datasources/notes_datasource.dart';
import 'package:umpisa_flutter/modules/notes/data/repository/notes_repo_impl.dart';
import 'package:umpisa_flutter/modules/notes/domain/repository/notes_repo.dart';
import 'package:umpisa_flutter/modules/notes/domain/usecases/add_note_uc.dart';
import 'package:umpisa_flutter/modules/notes/domain/usecases/delete_note_uc.dart';
import 'package:umpisa_flutter/modules/notes/domain/usecases/get_notes_uc.dart';
import 'package:umpisa_flutter/modules/notes/domain/usecases/update_note_uc.dart';

final sl = GetIt.instance;

Future<void> dependencyInjector() async {
  Dio dio = Dio();
  SecureStorageUtil secureStorageUtil = SecureStorageUtil();
  final Directory appDocDir = await getApplicationDocumentsDirectory();
  final String appDocPath = appDocDir.path;
  final cookieJar = PersistCookieJar(
    storage: FileStorage("$appDocPath/cookies"), // Specify the storage location
  );
  await secureStorageUtil.getInstance(kIsWeb);
  await EnvManager().load();
  sl
    ..registerFactory(() => NoteBloc(
        getNotesUC: sl(),
        deleteNoteUC: sl(),
        addNoteUC: sl(),
        updateNoteUC: sl()))
    ..registerLazySingleton(() => GetNotesUC(provider: sl()))
    ..registerLazySingleton(() => DeleteNoteUC(provider: sl()))
    ..registerLazySingleton(() => AddNoteUC(provider: sl()))
    ..registerLazySingleton(() => UpdateNoteUC(provider: sl()))
    ..registerLazySingleton<NotesRepository>(() => NotesRepoImpl(sl()))
    ..registerLazySingleton<NotesDataSource>(() => NodesDataSourceRemoteHttp(
        dio: sl(), cookieJar: sl(), storageUtil: sl()))

    //notes
    ..registerFactory(() => RegisterBloc(usecase: sl()))
    //usecases
    ..registerLazySingleton(() => RegisterUC(provider: sl()))
    ..registerFactory(() => AccountBloc(usecase: sl()))
    //usecases
    ..registerLazySingleton(() => LoginUC(provider: sl()))
    //provider
    ..registerLazySingleton<AccountsRepository>(() => AccountsRepoImp(sl()))
    //datasource
    ..registerLazySingleton<AccountsDataSource>(() =>
        AccountsDataSourceRemoteHttp(
            dio: sl(), secureStorageUtil: sl(), cookieJar: sl()))
    //httpClient
    ..registerLazySingleton<Dio>(() => dio)
    ..registerLazySingleton<PersistCookieJar>(() => cookieJar)
    ..registerLazySingleton<SecureStorageUtil>(() => secureStorageUtil);

  debugPrint('injected');
}
