import 'package:equatable/equatable.dart';
import 'package:umpisa_flutter/core/errors/failure_api_exception.dart';

abstract class Failure extends Equatable {
  final String message;
  final int statusCode;

  Failure({required this.message, required this.statusCode})
      : assert(
            statusCode.runtimeType == int || statusCode.runtimeType == String,
            'Status code cannot be ${statusCode.runtimeType}');
  String get errorMessage => 'Error $statusCode: $message';

  @override
  List<Object?> get props => [message, statusCode];
}

class ApiFailure extends Failure {
  ApiFailure({required super.message, required super.statusCode});

  ApiFailure.fromException(ApiServerException x)
      : this(message: x.message, statusCode: x.statusCode);
}

class InvalidDateFailure extends ApiFailure {
  // Named constructor with default values
  InvalidDateFailure() : super(message: "Invalid date!", statusCode: 104);
}
