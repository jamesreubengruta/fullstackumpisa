import 'package:equatable/equatable.dart';

class ApiServerException extends Equatable implements Exception {
  final String message;
  final int statusCode;

  const ApiServerException({required this.message, required this.statusCode});
  @override
  List<Object?> get props => [message, statusCode];
}
