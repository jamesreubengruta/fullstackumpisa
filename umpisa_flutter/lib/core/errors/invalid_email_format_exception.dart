class InvalidEmailFormatException implements Exception {
  final String message;

  InvalidEmailFormatException({this.message = "Invalid E-mail entered"});

  @override
  String toString() {
    return 'InvalidEmailFormatException: $message';
  }
}
