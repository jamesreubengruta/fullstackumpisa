import 'package:mocktail/mocktail.dart';
import 'package:umpisa_flutter/core/errors/failure_model.dart';

class MockAccountsFailure extends Mock implements ApiFailure {}
