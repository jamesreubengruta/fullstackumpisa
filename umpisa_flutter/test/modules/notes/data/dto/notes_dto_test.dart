import 'package:flutter_test/flutter_test.dart';
import 'package:umpisa_flutter/modules/notes/data/dto/notes_dto.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';

void main() {
  final NotesDTO dto = NotesDTO(
    id: '1',
    title: 'Test Note',
    note: 'This is a test note',
    description: 'Test description',
    date: '2024-04-19',
  );
  final NotesDTO emptyDTO = const NotesModel.empty().toModel();

  test('should be a subclass of NotesModel', () {
    expect(dto, isA<NotesModel>());
  });

  const noteJson =
      '{"id":"1","title":"Test Note","note":"This is a test note","description":"Test description","date":"2024-04-19"}';
  final noteMap = {
    'id': '1',
    'title': 'Test Note',
    'note': 'This is a test note',
    'description': 'Test description',
    'date': '2024-04-19',
  };

  group('fromMap', () {
    test('should return NotesDTO with correct data and not empty', () async {
      final result = NotesDTO.fromMap(noteMap);
      expect(result, isNot(emptyDTO));
    });
  });

  group('fromJson', () {
    test('should return NotesDTO with correct data and not empty', () async {
      final result = NotesDTO.fromJson(noteJson);
      expect(result, isNot(emptyDTO));
    });
  });

  group('toMap', () {
    test('should return correct map representation', () async {
      final result = dto.toMap();
      expect(result, noteMap);
    });
  });

  group('toJson', () {
    test('should return correct JSON representation', () async {
      final result = dto.toJson();
      expect(result, noteJson);
    });
  });

  group('copyWith', () {
    test('should return NotesDTO with proper field changed', () async {
      const String newTitle = 'Updated Test Note';
      final updatedDto = dto.copyWith(title: newTitle);
      expect(updatedDto.title, newTitle);
      expect(updatedDto.id, dto.id);
      expect(updatedDto.note, dto.note);
      expect(updatedDto.description, dto.description);
      expect(updatedDto.date, dto.date);
    });
  });

  group('toModel', () {
    test('should return NotesModel with same data as DTO', () async {
      final result = dto.toModel();
      expect(result.id, dto.id);
      expect(result.title, dto.title);
      expect(result.note, dto.note);
      expect(result.description, dto.description);
      expect(result.date, dto.date);
    });
  });
}
