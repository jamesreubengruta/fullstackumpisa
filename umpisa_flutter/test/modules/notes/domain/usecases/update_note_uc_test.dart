import 'package:flutter_test/flutter_test.dart';
import 'package:fpdart/fpdart.dart';
import 'package:mocktail/mocktail.dart';
import 'package:umpisa_flutter/core/errors/failure_model.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';
import 'package:umpisa_flutter/modules/notes/domain/repository/notes_repo.dart';
import 'package:umpisa_flutter/modules/notes/domain/usecases/update_note_uc.dart';

import 'mock.dart';

void main() {
  late UpdateNoteUC uc;
  late NotesRepository repo;

  const String noteId = 'note_id';
  const String updatedTitle = 'Updated Title';
  const String updatedNote = 'Updated Note';
  const String updateddescription = 'Updated desctiopn';
  const UpdateNoteParams updateParams = UpdateNoteParams(
      id: noteId,
      title: updatedTitle,
      note: updatedNote,
      description: updateddescription);
  late NotesModel updatedNoteModel;

  setUp(() {
    repo = MockNotesRepository();
    uc = UpdateNoteUC(provider: repo);
    updatedNoteModel = const NotesModel(
        id: noteId,
        title: updatedTitle,
        note: updatedNote,
        description: '',
        date: ''); // Creating an updated notes model
  });

  group('update note feature test', () {
    test(
        'should call [repo.updateNote], successful update should return updated notes model',
        () async {
      //stub
      when(() => repo.updateNote(
          id: noteId,
          title: updatedTitle,
          note: updatedNote)).thenAnswer((_) async => Right(updatedNoteModel));

      //act
      final result = await uc.call(updateParams);

      //verif
      expect(result, Right<Failure, NotesModel>(updatedNoteModel));
      verify(() => repo.updateNote(
          id: noteId, title: updatedTitle, note: updatedNote)).called(1);
      verifyNoMoreInteractions(repo);
    });

    test('should return a failure if update of note fails', () async {
      //act
      final failure =
          ApiFailure(message: 'Failed to update note', statusCode: 500);
      when(() => repo.updateNote(
          id: noteId,
          title: updatedTitle,
          note: updatedNote)).thenAnswer((_) async => Left(failure));

      //act
      final result = await uc.call(updateParams);

      //verif
      expect(result, Left<Failure, NotesModel>(failure));
      verify(() => repo.updateNote(
          id: noteId, title: updatedTitle, note: updatedNote)).called(1);
      verifyNoMoreInteractions(repo);
    });
  });
}
