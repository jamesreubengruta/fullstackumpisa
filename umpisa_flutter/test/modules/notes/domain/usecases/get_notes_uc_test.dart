import 'package:flutter_test/flutter_test.dart';
import 'package:fpdart/fpdart.dart';
import 'package:mocktail/mocktail.dart';
import 'package:umpisa_flutter/core/errors/failure_model.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';
import 'package:umpisa_flutter/modules/notes/domain/repository/notes_repo.dart';
import 'package:umpisa_flutter/modules/notes/domain/usecases/get_notes_uc.dart';

import 'mock.dart';

void main() {
  late GetNotesUC uc;
  late NotesRepository repo;

  const String categoryName = 'category_name';
  const GetNotesParams getParams = GetNotesParams(name: categoryName);
  late List<NotesModel> notes;

  setUp(() {
    repo = MockNotesRepository();
    uc = GetNotesUC(provider: repo);
    notes = List.generate(5, (index) => const NotesModel.empty());
  });

  group('get notes feature test', () {
    test(
        'should call [repo.getNotes], successful retrieval should return notes list',
        () async {
      //stub
      when(() => repo.getNotes(name: categoryName))
          .thenAnswer((_) async => Right(notes));

      //act
      final result = await uc.call(getParams);

      //verify
      expect(result, Right<Failure, List<NotesModel>>(notes));
      verify(() => repo.getNotes(name: categoryName)).called(1);
      verifyNoMoreInteractions(repo);
    });

    test('should return a failure if retrieval of notes fails', () async {
      //stub
      final failure =
          ApiFailure(message: 'Failed to retrieve notes', statusCode: 500);
      when(() => repo.getNotes(name: categoryName))
          .thenAnswer((_) async => Left(failure));

      //act
      final result = await uc.call(getParams);

      //verif
      expect(result, Left<Failure, List<NotesModel>>(failure));
      verify(() => repo.getNotes(name: categoryName)).called(1);
      verifyNoMoreInteractions(repo);
    });
  });
}
