import 'package:flutter_test/flutter_test.dart';
import 'package:fpdart/fpdart.dart';
import 'package:mocktail/mocktail.dart';
import 'package:umpisa_flutter/core/errors/failure_model.dart';
import 'package:umpisa_flutter/modules/notes/domain/models/notes_model.dart';
import 'package:umpisa_flutter/modules/notes/domain/repository/notes_repo.dart';
import 'package:umpisa_flutter/modules/notes/domain/usecases/delete_note_uc.dart';

import 'mock.dart';

void main() {
  late DeleteNoteUC uc;
  late NotesRepository repo;

  const String noteId = 'note_id';
  const DeleteNoteParams deleteParams = DeleteNoteParams(id: noteId);
  late List<NotesModel> notes;

  setUp(() {
    repo = MockNotesRepository();
    uc = DeleteNoteUC(provider: repo);
    notes = List.generate(
        5, (index) => const NotesModel.empty()); // Generating some sample notes
  });

  group('delete note feature test', () {
    test(
        'should call [repo.getNotes], successful deletion should return notes list',
        () async {
      // Stubbing the repo.getNotes to return a list of notes
      when(() => repo.getNotes(name: noteId))
          .thenAnswer((_) async => Right(notes));

      // Act
      final result = await uc.call(deleteParams);

      // Verify
      expect(result, Right<List<NotesModel>, void>(notes));
      verify(() => repo.getNotes(name: noteId))
          .called(1); // Ensure getNotes method is called once
      verifyNoMoreInteractions(
          repo); // Verify no further interactions with the repository
    });

    test('should return a failure if deleting note fails', () async {
      // Stubbing the repo.getNotes to return a failure
      final failure =
          ApiFailure(message: 'Failed to delete note', statusCode: 500);
      when(() => repo.getNotes(name: noteId))
          .thenAnswer((_) async => Left(failure));

      // Act
      final result = await uc.call(deleteParams);

      // Verify
      expect(result, Left<Failure, void>(failure));
      verify(() => repo.getNotes(name: noteId))
          .called(1); // Ensure getNotes method is called once
      verifyNoMoreInteractions(
          repo); // Verify no further interactions with the repository
    });
  });
}
