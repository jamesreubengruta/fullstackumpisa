import 'package:mocktail/mocktail.dart';
import 'package:umpisa_flutter/modules/notes/domain/repository/notes_repo.dart';

class MockNotesRepository extends Mock implements NotesRepository {}
