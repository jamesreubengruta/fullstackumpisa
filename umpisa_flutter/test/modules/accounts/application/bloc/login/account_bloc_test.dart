import 'package:flutter_test/flutter_test.dart';
import 'package:fpdart/fpdart.dart';
import 'package:mocktail/mocktail.dart';
import 'package:umpisa_flutter/core/errors/failure_model.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/login/account_bloc.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/login/account_event.dart';
import 'package:umpisa_flutter/modules/accounts/application/bloc/login/account_state.dart';
import 'package:umpisa_flutter/modules/accounts/domain/models/account_model.dart';
import 'package:umpisa_flutter/modules/accounts/domain/usecases/login_uc.dart';

class MockLoginUC extends Mock implements LoginUC {}

void main() {
  late MockLoginUC mockLoginUC;
  late AccountBloc accountBloc;
  const String email = 'test@example.com';
  const String password = 'password';
  const AccountModel accountModel = AccountModel(
      id: 1,
      name: 'Test User',
      email: email,
      password: 'password',
      description: 'desription');

  setUp(() {
    mockLoginUC = MockLoginUC();
    accountBloc = AccountBloc(usecase: mockLoginUC);
  });

  tearDown(() {
    accountBloc.close();
  });

  test('initial state should be LoginInitialState', () {
    expect(accountBloc.state, const LoginInitialState());
  });

  group('LoginAccountEvent', () {
    test('emits [LogginInState, LoginSuccessState] when login is successful',
        () {
      when(() => mockLoginUC(any()))
          .thenAnswer((_) async => const Right(accountModel));

      final expectedStates = [
        const LogginInState(),
        const LoginSuccessState(model: accountModel),
      ];

      expectLater(accountBloc.stream, emitsInOrder(expectedStates));

      accountBloc
          .add(const LoginAccountEvent(email: email, password: password));
    });

    test('emits [LogginInState, LoginErrorState] when login fails', () {
      final apiFailure =
          ApiFailure(message: 'Invalid credentials', statusCode: 401);
      when(() => mockLoginUC(any())).thenAnswer((_) async => Left(apiFailure));

      final expectedStates = [
        const LogginInState(),
        LoginErrorState(message: apiFailure.errorMessage),
      ];

      expectLater(accountBloc.stream, emitsInOrder(expectedStates));

      accountBloc
          .add(const LoginAccountEvent(email: email, password: password));
    });
  });
}
