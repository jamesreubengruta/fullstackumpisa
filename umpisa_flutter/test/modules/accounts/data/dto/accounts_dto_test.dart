import 'package:flutter_test/flutter_test.dart';
import 'package:umpisa_flutter/modules/accounts/data/dto/accounts_dto.dart';
import 'package:umpisa_flutter/modules/accounts/domain/models/account_model.dart';

void main() {
  const AccountsDTO dto = AccountsDTO(
    id: 0,
    name: 'John Doe',
    email: 'john@example.com',
    password: 'password123',
    description: 'Test account',
  );

  final AccountsDTO emptyDto = const AccountModel.empty().toModel();

  test('should be a subclass of AccountModel', () {
    expect(dto, isA<AccountModel>());
  });

  const accountJson =
      '{"id":"1","name":"John Doe","email":"john@example.com","password":"password123","description":"Test account"}';
  final accountMap = {
    'id': '1',
    'name': 'John Doe',
    'email': 'john@example.com',
    'password': 'password123',
    'description': 'Test account',
  };

  group('fromMap', () {
    test('should return AccountsDTO with correct data and not empty', () async {
      final result = AccountsDTO.fromMap(accountMap);
      expect(result, isNot(emptyDto));
    });
  });

  group('fromJson', () {
    test('should return AccountsDTO with correct data and not empty', () async {
      final result = AccountsDTO.fromJson(accountJson);
      expect(result, isNot(emptyDto));
      expect(result.id, accountMap['id']);
      expect(result.name, accountMap['name']);
      expect(result.email, accountMap['email']);
      expect(result.password, accountMap['password']);
      expect(result.description, accountMap['description']);
    });
  });

  group('toMap', () {
    test('should return correct map representation', () async {
      final result = dto.toMap();
      expect(result, accountMap);
    });
  });

  group('toJson', () {
    test('should return correct JSON representation', () async {
      final result = dto.toJson();
      expect(result, accountJson);
    });
  });

  group('toModel', () {
    test('should return AccountModel with same data as DTO', () async {
      final result = dto.toModel();
      expect(result.id, dto.id);
      expect(result.name, dto.name);
      expect(result.email, dto.email);
      expect(result.password, dto.password);
      expect(result.description, dto.description);
    });
  });
}
