import 'package:mocktail/mocktail.dart';
import 'package:umpisa_flutter/modules/accounts/domain/repository/accounts_repo.dart';

class MockAccountsRep extends Mock implements AccountsRepository {}
