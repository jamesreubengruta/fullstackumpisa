import 'package:flutter_test/flutter_test.dart';
import 'package:fpdart/fpdart.dart';
import 'package:mocktail/mocktail.dart';
import 'package:umpisa_flutter/core/core.dart';
import 'package:umpisa_flutter/core/errors/failure_model.dart';
import 'package:umpisa_flutter/core/errors/invalid_email_format_exception.dart';
import 'package:umpisa_flutter/modules/accounts/domain/models/account_model.dart';
import 'package:umpisa_flutter/modules/accounts/domain/repository/accounts_repo.dart';
import 'package:umpisa_flutter/modules/accounts/domain/usecases/login_uc.dart';

import 'mock.dart';

void main() {
  late LoginUC uc;
  late AccountsRepository repo;
  //params

  const AccountModel am = AccountModel.empty();
  const LoginNoteParams loginParams =
      LoginNoteParams(email: 'valid@gmail.com', password: 'Qwerty12345!');
  const LoginNoteParams loginParamsFailure =
      LoginNoteParams(email: 'valid@emailcom', password: 'Qwerty12345!');
  late ApiFailure failure;
  final InvalidEmailFormatException invalidEmail =
      InvalidEmailFormatException();

  setUp(() {
    repo = MockAccountsRep();
    uc = LoginUC(provider: repo);
    failure = ApiFailure(message: '', statusCode: 400);
  });

  group('login feature test', () {
    test(
        'should call [repo.login], successful login should return AccountsModel',
        () async {
      //stub
      when(
        () => repo.login(
            email: any(named: 'email'), password: any(named: 'password')),
      ).thenAnswer((invocation) async => const Right(am));
      //act
      final result = await uc.call(loginParams);
      //verify
      expect(result, const Right<dynamic, AccountModel>(am));
      verify(
        () => repo.login(
            email: any(named: 'email'), password: any(named: 'password')),
      );
      verifyNoMoreInteractions(repo);
    });
    test('should call [repo.login] failed login should return Failure object',
        () async {
      //stub
      when(
        () => repo.login(
            email: any(named: 'email'), password: any(named: 'password')),
      ).thenAnswer((invocation) async => Left(failure));
      //act
      final result = await uc.call(loginParams);
      //verify
      expect(result,
          Left<Failure, ApiFailure>(ApiFailure(message: '', statusCode: 400)));
      verify(
        () => repo.login(
            email: any(named: 'email'), password: any(named: 'password')),
      );
      verifyNoMoreInteractions(repo);
    });

    test('should throw an error if email is invalid', () async {
      //stub
      when(
        () => repo.login(
            email: any(named: 'email'), password: any(named: 'password')),
      ).thenThrow(invalidEmail);

      //act
      expect(() => uc.call(loginParamsFailure),
          throwsA(isA<InvalidEmailFormatException>()));

      //verify login is not called because error was thrown
      verifyNever(
        () => repo.login(
            email: any(named: 'email'), password: any(named: 'password')),
      ).called(0);

      verifyNoMoreInteractions(repo);
    });
  });
}
