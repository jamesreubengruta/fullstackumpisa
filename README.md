# FullStackUmpisa

Project consists of Flutter frontend and Node(NestJS) Backend.
Due to time constraints some very minor issues like usage of static strings, some code duplications and lack of unit tests exists. But industry standards like CLEAN architecture and SOLID principles implemented. The app looks very simple but it works effectively.

# Requirements

- Flutter version 3.19.4 or versionInstall FVM from https://fvm.app and install flutter from there.
- Node version 19.8.1 or install NVM from https://github.com/nvm-sh/nvm and install node from there.

# Running the project

Run the server:

- Go to umpisa_backend directory then run "pnpm install" to install dependencies.
- Then run "pnpm run start" to start the server. Make sure port 3000 is free or else error will occur.
- To confirm if server is running: visit "localhost:3000" in your browser and you should see Hello World message.

Run the app.

- Go to umpisa_flutter then run "fvm flutter run".
- Create an account using the register button.
- Login with your new account using email and password
- After successful login you should see the main dashboard.
- Main dashboard consists of 2 tabs, first is for Notes and second is for profile.
