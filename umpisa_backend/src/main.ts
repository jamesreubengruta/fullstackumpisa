import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as cookieParser from 'cookie-parser';
import * as session from 'express-session';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(cookieParser());
  app.use(
    session({
      secret: 'secret-from-umpisa-lol',
      resave: false,
      saveUninitialized: false,
      cookie: {
        secure: false, // Set to true if your app is served over HTTPS
        httpOnly: true, // This prevents client-side access to the cookie
        maxAge: 3600000, // Cookie expiration time in milliseconds (e.g., 1 hour)
      },
    }),
  );

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );
  await app.listen(3000);
}
bootstrap();
