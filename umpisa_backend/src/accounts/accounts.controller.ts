import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
  Session,
} from '@nestjs/common';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { AccountsService } from './accounts.service';
import { AuthService } from './auth.service';
import { CurrentUser } from './decorators/current-user.decorator';
import { AccountDTO } from './dtos/account.dto';
import { CreateAccountDTO } from './dtos/create-account.dto';
import { LoginAccountDTO } from './dtos/login-account.dto';
import { UpdateAccountDTO } from './dtos/update-account.dto';
@Controller('auth')
@Serialize(AccountDTO)
export class AccountsController {
  constructor(
    private accountService: AccountsService,
    private authService: AuthService,
  ) {}

  @Get('/whois')
  whoIs(@CurrentUser() user: string) {
    return user;
  }

  @Get('/signout')
  logOut(@Session() session: any) {
    console.log('exit');
    session.userId = null;
  }

  @Post('/signup')
  async createAccount(@Body() body: CreateAccountDTO, @Session() session: any) {
    const user = await this.authService.register(
      body.name,
      body.email,
      body.password,
      body.description,
    );
    session.userId = user.id;

    return user;
  }

  @Post('/login')
  async login(@Body() body: LoginAccountDTO, @Session() session: any) {
    const user = await this.authService.login(body.email, body.password);
    session.userId = user.id;

    return user;
  }

  @Get('/:id')
  async findAccount(@Param('id') id: string) {
    const user = await this.accountService.findOne(parseInt(id));
    if (!user) {
      throw new NotFoundException();
    }

    return user;
  }
  @Get()
  findAllUsers(@Query('email') email: string) {
    return this.accountService.find(email);
  }

  @Delete('/:id')
  delete(@Param('id') id: string) {
    return this.accountService.remove(parseInt(id));
  }

  @Patch('/:id')
  update(@Param('id') id: string, @Body() body: UpdateAccountDTO) {
    return this.accountService.update(parseInt(id), body);
  }
}
