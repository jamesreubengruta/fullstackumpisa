import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Account } from './accounts.entity';
@Injectable()
export class AccountsService {
  constructor(@InjectRepository(Account) private repo: Repository<Account>) {}
  userError: string = 'user not found';

  create(name: string, email: string, password: string, description: string) {
    const account = this.repo.create({ name, email, password, description });

    return this.repo.save(account);
  }

  findOne(id: number) {
    if (!id) {
      return null;
    }
    const user = this.repo.findOneBy({ id });
    if (!user) {
      throw new NotFoundException(this.userError);
    }
    return user;
  }
  find(email: string) {
    return this.repo.find({ where: { email } });
  }

  async update(id: number, any: Partial<Account>) {
    const user = await this.findOne(id);
    if (!user) {
      throw new NotFoundException(this.userError);
    }
    Object.assign(user, any);
    return this.repo.save(user);
  }

  async remove(id: number) {
    const user = await this.findOne(id);
    if (!user) {
      throw new NotFoundException(this.userError);
    }
    return this.repo.remove(user);
  }
}
