import { BadRequestException, Injectable } from '@nestjs/common';
import { scrypt as _scrypt, randomBytes } from 'crypto';
import { promisify } from 'util';
import { AccountsService } from './accounts.service';

const scrypt = promisify(_scrypt);

@Injectable()
export class AuthService {
  constructor(private service: AccountsService) {}

  async register(
    name: string,
    email: string,
    password: string,
    description: string,
  ) {
    //check if email existst
    const users = await this.service.find(email);
    if (users.length) {
      throw new BadRequestException('Email already taken');
    }
    //generate salt

    const salt = randomBytes(8).toString('hex');

    //hash
    const hash = (await scrypt(password, salt, 32)) as Buffer;

    //combine hash and salt
    const result = salt + '.' + hash.toString('hex');

    //create new user and save
    const user = await this.service.create(name, email, result, description);
    //return the user
    return user;
  }

  async login(email: string, password: string) {
    //check if email existst
    const [user] = await this.service.find(email);
    if (!user) {
      throw new BadRequestException('Invalid Email');
    }
    //hash the password

    //get hash and salt
    const passFromDB = user['password'];
    const [saltFromDB, hashFromDB] = passFromDB.split('.');

    const hash = (await scrypt(password, saltFromDB, 32)) as Buffer;

    const success = hashFromDB === hash.toString('hex');

    if (!success) {
      throw new BadRequestException('Incorrect credentials');
    }
    return user;
  }
}
