import { Note } from 'src/notes/notes.entity';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
@Entity()
export class Account {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  email: string;
  @Column()
  password: string;
  @Column()
  description: string;
  @OneToMany(() => Note, (note) => note.accountId)
  notes: Note[];

  @AfterInsert()
  logInsert() {
    console.log('inserted', this.id);
  }

  @AfterUpdate()
  logUpdate() {
    console.log('updated', this.id);
  }

  @AfterRemove()
  logRemove() {
    console.log('removed ', this.id);
  }
}
