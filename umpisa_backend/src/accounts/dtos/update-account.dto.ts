import { IsEmail, IsOptional, IsString } from 'class-validator';

export class UpdateAccountDTO {
  @IsEmail()
  @IsOptional()
  email: string;

  @IsString()
  @IsOptional()
  password: string;

  @IsString()
  @IsOptional()
  description: string;

  @IsString()
  @IsOptional()
  name: string;
}
