import { Expose } from 'class-transformer';

export class AccountDTO {
  @Expose()
  id: number;
  @Expose()
  name: string;
  @Expose()
  email: string;
  @Expose()
  description: string;
}
