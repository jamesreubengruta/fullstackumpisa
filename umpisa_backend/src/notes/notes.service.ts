import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Account } from 'src/accounts/accounts.entity';
import { Repository } from 'typeorm';
import { CreateNoteDTO } from './dtos/create-note.dto';
import { Note } from './notes.entity';
@Injectable()
export class NotesService {
  constructor(@InjectRepository(Note) private repo: Repository<Note>) {}

  create(dto: CreateNoteDTO, user: Account) {
    const note = this.repo.create(dto);
    note.accountId = user;
    return this.repo.save(note);
  }

  get(user: Account) {
    const note = this.repo.find({ where: { accountId: user } });
    return note;
  }

  async remove(user: Account, noteId: number) {
    const note = await this.repo.find({
      where: { accountId: user, id: noteId },
    });
    if (!note) {
      throw new NotFoundException('no such thing exists!');
    }
    return this.repo.remove(note);
  }

  findOne(id: number) {
    if (!id) {
      return null;
    }
    const note = this.repo.findOneBy({ id });
    if (!note) {
      throw new NotFoundException('no such thing');
    }
    return note;
  }

  async update(user: Account, id: number, payload: Partial<Note>) {
    const note = await this.repo.findOne({
      where: { accountId: user, id: id },
    });

    Object.assign(note, payload);
    return this.repo.save(note);
  }
}
