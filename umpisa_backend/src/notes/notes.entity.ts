import { Account } from 'src/accounts/accounts.entity';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Note {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  title: string;
  @Column()
  note: string;
  @Column()
  description: string;
  @Column()
  date: string;
  @ManyToOne(() => Account, (account) => account.notes)
  accountId: Account;

  @AfterInsert()
  logInsert() {
    console.log('inserted', this.id);
  }

  @AfterUpdate()
  logUpdate() {
    console.log('updated', this.id);
  }

  @AfterRemove()
  logRemove() {
    console.log('removed ', this.id);
  }
}
