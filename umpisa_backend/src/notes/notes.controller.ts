import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { Account } from 'src/accounts/accounts.entity';
import { CurrentUser } from 'src/accounts/decorators/current-user.decorator';
import { AuthGuard } from 'src/guards/auth.guard';
import { CreateNoteDTO } from './dtos/create-note.dto';
import { UpdateNoteDTO } from './dtos/update-note.dto';
import { NotesService } from './notes.service';
@Controller('notes')
export class NotesController {
  constructor(private service: NotesService) {}

  @Post()
  @UseGuards(AuthGuard)
  createNote(@Body() body: CreateNoteDTO, @CurrentUser() user: Account) {
    return this.service.create(body, user);
  }

  @Get()
  @UseGuards(AuthGuard)
  getNotes(@CurrentUser() user: Account) {
    return this.service.get(user);
  }

  @Delete('/:id')
  @UseGuards(AuthGuard)
  deleteNote(@CurrentUser() user: Account, @Param('id') noteId: string) {
    console.log('controller noteId', noteId);
    return this.service.remove(user, parseInt(noteId));
  }

  @Patch('/:id')
  @UseGuards(AuthGuard)
  update(
    @CurrentUser() user: Account,
    @Param('id') id: string,
    @Body() body: UpdateNoteDTO,
  ) {
    return this.service.update(user, parseInt(id), body);
  }
}
