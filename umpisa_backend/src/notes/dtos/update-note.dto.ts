import { IsOptional, IsString } from 'class-validator';

export class UpdateNoteDTO {
  @IsString()
  @IsOptional()
  title: string;

  @IsString()
  @IsOptional()
  note: string;

  @IsString()
  @IsOptional()
  description: string;

  @IsString()
  @IsOptional()
  date: string;
}
