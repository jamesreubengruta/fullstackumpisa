import { IsString } from 'class-validator';

export class CreateNoteDTO {
  @IsString()
  title: string;
  @IsString()
  note: string;
  @IsString()
  description: string;
  @IsString()
  date: string;
}
