export declare class CreateAccountDTO {
    name: string;
    email: string;
    password: string;
    description: string;
}
