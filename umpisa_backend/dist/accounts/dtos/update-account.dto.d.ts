export declare class UpdateAccountDTO {
    email: string;
    password: string;
    description: string;
    name: string;
}
