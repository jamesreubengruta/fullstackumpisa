export declare class AccountDTO {
    id: number;
    name: string;
    email: string;
    description: string;
}
