import { Repository } from 'typeorm';
import { Account } from './accounts.entity';
export declare class AccountsService {
    private repo;
    constructor(repo: Repository<Account>);
    userError: string;
    create(name: string, email: string, password: string, description: string): Promise<Account>;
    findOne(id: number): Promise<Account>;
    find(email: string): Promise<Account[]>;
    update(id: number, any: Partial<Account>): Promise<Account>;
    remove(id: number): Promise<Account>;
}
