import { AccountsService } from './accounts.service';
export declare class AuthService {
    private service;
    constructor(service: AccountsService);
    register(name: string, email: string, password: string, description: string): Promise<import("./accounts.entity").Account>;
    login(email: string, password: string): Promise<import("./accounts.entity").Account>;
}
