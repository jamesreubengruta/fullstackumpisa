import { Note } from 'src/notes/notes.entity';
export declare class Account {
    id: number;
    name: string;
    email: string;
    password: string;
    description: string;
    notes: Note[];
    logInsert(): void;
    logUpdate(): void;
    logRemove(): void;
}
