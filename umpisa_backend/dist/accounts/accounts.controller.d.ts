import { AccountsService } from './accounts.service';
import { AuthService } from './auth.service';
import { CreateAccountDTO } from './dtos/create-account.dto';
import { LoginAccountDTO } from './dtos/login-account.dto';
import { UpdateAccountDTO } from './dtos/update-account.dto';
export declare class AccountsController {
    private accountService;
    private authService;
    constructor(accountService: AccountsService, authService: AuthService);
    whoIs(user: string): string;
    logOut(session: any): void;
    createAccount(body: CreateAccountDTO, session: any): Promise<import("./accounts.entity").Account>;
    login(body: LoginAccountDTO, session: any): Promise<import("./accounts.entity").Account>;
    findAccount(id: string): Promise<import("./accounts.entity").Account>;
    findAllUsers(email: string): Promise<import("./accounts.entity").Account[]>;
    delete(id: string): Promise<import("./accounts.entity").Account>;
    update(id: string, body: UpdateAccountDTO): Promise<import("./accounts.entity").Account>;
}
