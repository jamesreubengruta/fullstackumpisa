"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountsController = void 0;
const common_1 = require("@nestjs/common");
const serialize_interceptor_1 = require("../interceptors/serialize.interceptor");
const accounts_service_1 = require("./accounts.service");
const auth_service_1 = require("./auth.service");
const current_user_decorator_1 = require("./decorators/current-user.decorator");
const account_dto_1 = require("./dtos/account.dto");
const create_account_dto_1 = require("./dtos/create-account.dto");
const login_account_dto_1 = require("./dtos/login-account.dto");
const update_account_dto_1 = require("./dtos/update-account.dto");
let AccountsController = class AccountsController {
    constructor(accountService, authService) {
        this.accountService = accountService;
        this.authService = authService;
    }
    whoIs(user) {
        return user;
    }
    logOut(session) {
        console.log('exit');
        session.userId = null;
    }
    async createAccount(body, session) {
        const user = await this.authService.register(body.name, body.email, body.password, body.description);
        session.userId = user.id;
        return user;
    }
    async login(body, session) {
        const user = await this.authService.login(body.email, body.password);
        session.userId = user.id;
        return user;
    }
    async findAccount(id) {
        const user = await this.accountService.findOne(parseInt(id));
        if (!user) {
            throw new common_1.NotFoundException();
        }
        return user;
    }
    findAllUsers(email) {
        return this.accountService.find(email);
    }
    delete(id) {
        return this.accountService.remove(parseInt(id));
    }
    update(id, body) {
        return this.accountService.update(parseInt(id), body);
    }
};
exports.AccountsController = AccountsController;
__decorate([
    (0, common_1.Get)('/whois'),
    __param(0, (0, current_user_decorator_1.CurrentUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], AccountsController.prototype, "whoIs", null);
__decorate([
    (0, common_1.Get)('/signout'),
    __param(0, (0, common_1.Session)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AccountsController.prototype, "logOut", null);
__decorate([
    (0, common_1.Post)('/signup'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Session)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_account_dto_1.CreateAccountDTO, Object]),
    __metadata("design:returntype", Promise)
], AccountsController.prototype, "createAccount", null);
__decorate([
    (0, common_1.Post)('/login'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Session)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [login_account_dto_1.LoginAccountDTO, Object]),
    __metadata("design:returntype", Promise)
], AccountsController.prototype, "login", null);
__decorate([
    (0, common_1.Get)('/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AccountsController.prototype, "findAccount", null);
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Query)('email')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], AccountsController.prototype, "findAllUsers", null);
__decorate([
    (0, common_1.Delete)('/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], AccountsController.prototype, "delete", null);
__decorate([
    (0, common_1.Patch)('/:id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_account_dto_1.UpdateAccountDTO]),
    __metadata("design:returntype", void 0)
], AccountsController.prototype, "update", null);
exports.AccountsController = AccountsController = __decorate([
    (0, common_1.Controller)('auth'),
    (0, serialize_interceptor_1.Serialize)(account_dto_1.AccountDTO),
    __metadata("design:paramtypes", [accounts_service_1.AccountsService,
        auth_service_1.AuthService])
], AccountsController);
//# sourceMappingURL=accounts.controller.js.map