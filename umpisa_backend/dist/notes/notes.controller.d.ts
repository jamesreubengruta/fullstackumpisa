import { Account } from 'src/accounts/accounts.entity';
import { CreateNoteDTO } from './dtos/create-note.dto';
import { UpdateNoteDTO } from './dtos/update-note.dto';
import { NotesService } from './notes.service';
export declare class NotesController {
    private service;
    constructor(service: NotesService);
    createNote(body: CreateNoteDTO, user: Account): Promise<import("./notes.entity").Note>;
    getNotes(user: Account): Promise<import("./notes.entity").Note[]>;
    deleteNote(user: Account, noteId: string): Promise<import("./notes.entity").Note[]>;
    update(user: Account, id: string, body: UpdateNoteDTO): Promise<import("./notes.entity").Note>;
}
