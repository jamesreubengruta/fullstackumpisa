import { Account } from 'src/accounts/accounts.entity';
export declare class Note {
    id: number;
    title: string;
    note: string;
    description: string;
    date: string;
    accountId: Account;
    logInsert(): void;
    logUpdate(): void;
    logRemove(): void;
}
