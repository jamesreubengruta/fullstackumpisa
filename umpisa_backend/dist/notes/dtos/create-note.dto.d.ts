export declare class CreateNoteDTO {
    title: string;
    note: string;
    description: string;
    date: string;
}
