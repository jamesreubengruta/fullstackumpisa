export declare class UpdateNoteDTO {
    title: string;
    note: string;
    description: string;
    date: string;
}
