import { Account } from 'src/accounts/accounts.entity';
import { Repository } from 'typeorm';
import { CreateNoteDTO } from './dtos/create-note.dto';
import { Note } from './notes.entity';
export declare class NotesService {
    private repo;
    constructor(repo: Repository<Note>);
    create(dto: CreateNoteDTO, user: Account): Promise<Note>;
    get(user: Account): Promise<Note[]>;
    remove(user: Account, noteId: number): Promise<Note[]>;
    findOne(id: number): Promise<Note>;
    update(user: Account, id: number, payload: Partial<Note>): Promise<Note>;
}
